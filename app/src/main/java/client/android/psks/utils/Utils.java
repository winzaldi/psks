package client.android.psks.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import client.android.psks.R;
import client.android.psks.model.quitioner.Entities;

/**
 * Created by winzaldi on 11/16/17.
 */

public class Utils {

    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);

    public static String getDeviceIMEI(Context context) {
        String deviceUniqueIdentifier = null;
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (null != tm) {
            deviceUniqueIdentifier = tm.getDeviceId();
        }
        if (null == deviceUniqueIdentifier || 0 == deviceUniqueIdentifier.length()) {
            deviceUniqueIdentifier = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        }
        return deviceUniqueIdentifier;
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public static int generateViewId() {

        if (Build.VERSION.SDK_INT < 17) {
            for (; ; ) {
                final int result = sNextGeneratedId.get();
                // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
                int newValue = result + 1;
                if (newValue > 0x00FFFFFF)
                    newValue = 1; // Roll over to 1, not 0.
                if (sNextGeneratedId.compareAndSet(result, newValue)) {
                    return result;
                }
            }
        } else {
            return View.generateViewId();
        }

    }

    public static Bundle toBundle(Map<String, String> input) {
        Bundle output = new Bundle();
        for (String key : input.keySet()) {
            output.putString(key, input.get(key));
        }
        return output;
    }

    public static HashMap<String, String> fromBundle(Bundle input) {
        HashMap<String, String> output = new HashMap<>();
        for (String key : input.keySet()) {
            output.put(key, input.getString(key));
        }
        return output;
    }

    //    public static int countSections(Entities[] entities) {
//        int result = 0;
//        for (Entities e : entities) {
//            if (e.getQuestionType().equalsIgnoreCase("SECTION")) {
//                result++;
//            }
//        }
//        return result;
//    }
//    public static String encodeFiletoBase64string(String pathFile) {
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        Bitmap bitmap = BitmapFactory.decodeFile(pathFile);
//        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
//        byte[] imageBytes = baos.toByteArray();
//        return Base64.encodeToString(imageBytes, Base64.DEFAULT);
//    }

    public static String encodeFileToBase64Binary(String fileName){
        File file = new File(fileName);
        byte[] bytes = loadFile(file);
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }

    private static byte[] loadFile(File file) {
        byte[] bytes = null;
        try {
            InputStream is = new FileInputStream(file);

            long length = file.length();
            if (length > Integer.MAX_VALUE) {
                // File is too large
            }
            bytes = new byte[(int) length];

            int offset = 0;
            int numRead = 0;
            while (offset < bytes.length
                    && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
                offset += numRead;
            }

            if (offset < bytes.length) {
                throw new IOException("Could not completely read file " + file.getName());
            }

            is.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return bytes;
    }


    public static HashMap<Integer, Entities[]> splitQuestionEntities(Entities[] entities) {
        List<Entities[]> list = new ArrayList<>();
        HashMap<Integer, Entities[]> hashMap = new HashMap<>();
        int start = 0;
        int key = 0;
        for (int idx = 0; idx < entities.length; idx++) {
            Entities e = entities[idx];
            if (e.getQuestionType().equalsIgnoreCase("SECTION")) {
                System.out.println(e.getQuestionType());
                hashMap.put(key, Arrays.copyOfRange(entities, start, idx + 1));
                start = idx + 1;
                key = key + 1;
            }

            if (idx == entities.length - 1) {
                if (start <= idx) {
                    hashMap.put(key, Arrays.copyOfRange(entities, start, idx + 1));
                }
            }
        }
        return hashMap;
    }

    public static void showMessageDialog(Spanned message, String title, Context context) {
        try {
            TextView messageTextView = new TextView(context);
            messageTextView.setText(message);
            messageTextView.setMovementMethod(LinkMovementMethod.getInstance());
            messageTextView.setPadding(100, 20, 60, 10);

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(title);
            builder.setView(messageTextView);
            builder.setPositiveButton(context.getResources().getString(R.string.alert_dialog_ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        } catch (final Exception e) {
            Log.d(Utils.class.getSimpleName(), "showAlertDialog(): Failed.", e);
        }
    }

    public static String dateToString(Date date,String formatDate){
        SimpleDateFormat sdf = new SimpleDateFormat(formatDate);
        return  sdf.format(date);

    }
    public static Date stringToDate(String date,String formatDate) {
        SimpleDateFormat sdf = new SimpleDateFormat(formatDate);
        try {
            return  sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] getBytesFromFile(File file) throws IOException {
        // Get the size of the file
        long length = file.length();

        // You cannot create an array using a long type.
        // It needs to be an int type.
        // Before converting to an int type, check
        // to ensure that file is not larger than Integer.MAX_VALUE.
        if (length > Integer.MAX_VALUE) {
            // File is too large
            throw new IOException("File is too large!");
        }

        // Create the byte array to hold the data
        byte[] bytes = new byte[(int)length];

        // Read in the bytes
        int offset = 0;
        int numRead = 0;

        InputStream is = new FileInputStream(file);
        try {
            while (offset < bytes.length
                    && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
                offset += numRead;
            }
        } finally {
            is.close();
        }

        // Ensure all the bytes have been read in
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file "+file.getName());
        }
        return bytes;
    }


    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private static final AtomicLong LAST_TIME_MS = new AtomicLong();
    public static long uniqueCurrentTimeMS() {
        long now = System.currentTimeMillis();
        while(true) {
            long lastTime = LAST_TIME_MS.get();
            if (lastTime >= now)
                now = lastTime+1;
            if (LAST_TIME_MS.compareAndSet(lastTime, now))
                return now;
        }
    }

    public static void showSnakbarMessage(View rootView, String mMessage) {
        Snackbar.make(rootView, mMessage, Snackbar.LENGTH_LONG)
                .setAction("Action", null)
                .show();
    }

    public static void showSnakbarMessageOK(View rootView, String mMessage) {
        Snackbar.make(rootView, mMessage, Snackbar.LENGTH_LONG)
                .setAction("OK", null)
                .show();
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }


//    Snackbar snackbar = Snackbar.make(view,"TXT",Snackbar.LENGTH_LONG).setAction("OK", new View.OnClickListener() {
//        @Override
//        public void onClick(View v) { }
//    });


//    public static void main(String[] args) {
//        Entities[] entities = FakeUtils.getQuestioner(FakeUtils.FORM_JSON_2).getEntities();
//
//        System.out.println(countSections(entities));
//        System.out.println(splitQuestionEntities(entities).size());
//        List<Entities[]> list = splitQuestionEntities(entities);
//        for (Entities[] e : list) {
//            for (Entities en : e) {
//                System.out.println(en);
//
//            }
//            System.out.println("======================");
//        }
//
//
//    }

}
