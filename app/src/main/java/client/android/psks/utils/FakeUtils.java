package client.android.psks.utils;

import com.google.gson.Gson;

import client.android.psks.model.member.PKSMemberResponse;
import client.android.psks.model.question.QuestionDetailResponse;
import client.android.psks.model.quitioner.QuestionnaireResponse;
import client.android.psks.model.survey.SurveyAssignmentResponse;

/**
 * Created by winzaldi on 11/19/17.
 */

public class FakeUtils {

    public static final String FORM_JSON_2 = "{\n" +
            "    \"entities\": [\n" +
            "        {\n" +
            "            \"required\": true,\n" +
            "            \"questionnaireQuestionId\": 500001,\n" +
            "            \"questionType\": \"SHORT_TEXT\",\n" +
            "            \"usedDataValidation\": false,\n" +
            "            \"questionDesc\": \"\",\n" +
            "            \"usedDescription\": false,\n" +
            "            \"questionName\": \"Siapa\",\n" +
            "            \"shuffleOptionOrder\": false\n" +
            "        },\n" +
            "        {\n" +
            "            \"required\": true,\n" +
            "            \"questionnaireQuestionId\": 500014,\n" +
            "            \"questionType\": \"PARAGRAPH\",\n" +
            "            \"usedDataValidation\": false,\n" +
            "            \"questionDesc\": \"\",\n" +
            "            \"usedDescription\": false,\n" +
            "            \"questionName\": \"Siapa1\",\n" +
            "            \"shuffleOptionOrder\": false\n" +
            "        },\n" +
            "        {\n" +
            "            \"required\": true,\n" +
            "            \"questionnaireQuestionId\": 500015,\n" +
            "            \"questionType\": \"DATE\",\n" +
            "            \"usedDataValidation\": false,\n" +
            "            \"questionDesc\": \"\",\n" +
            "            \"usedDescription\": false,\n" +
            "            \"questionName\": \"Siapa2\",\n" +
            "            \"shuffleOptionOrder\": false\n" +
            "        },\n" +
            "        {\n" +
            "            \"required\": true,\n" +
            "            \"questionnaireQuestionId\": 500016,\n" +
            "            \"questionType\": \"TIME\",\n" +
            "            \"usedDataValidation\": false,\n" +
            "            \"questionDesc\": \"\",\n" +
            "            \"usedDescription\": false,\n" +
            "            \"questionName\": \"Siapa3\",\n" +
            "            \"shuffleOptionOrder\": false\n" +
            "        },\n" +
            "        {\n" +
            "            \"required\": true,\n" +
            "            \"questionnaireQuestionId\": 500018,\n" +
            "            \"questionType\": \"DROPDOWN\",\n" +
            "            \"usedDataValidation\": false,\n" +
            "            \"questionDesc\": \"\",\n" +
            "            \"usedDescription\": false,\n" +
            "            \"questionName\": \"Siapa5\",\n" +
            "            \"shuffleOptionOrder\": false\n" +
            "        },\n" +
            "        {\n" +
            "            \"required\": true,\n" +
            "            \"questionnaireQuestionId\": 500019,\n" +
            "            \"questionType\": \"CHECKBOX\",\n" +
            "            \"usedDataValidation\": false,\n" +
            "            \"questionDesc\": \"\",\n" +
            "            \"usedDescription\": false,\n" +
            "            \"questionName\": \"Siapa6\",\n" +
            "            \"shuffleOptionOrder\": false\n" +
            "        },\n" +
            "        {\n" +
            "            \"required\": true,\n" +
            "            \"questionnaireQuestionId\": 500020,\n" +
            "            \"questionType\": \"SECTION\",\n" +
            "            \"usedDataValidation\": false,\n" +
            "            \"questionDesc\": \"\",\n" +
            "            \"usedDescription\": false,\n" +
            "            \"questionName\": \"Siapa7\",\n" +
            "            \"shuffleOptionOrder\": false\n" +
            "        },\n" +
            "        {\n" +
            "            \"required\": true,\n" +
            "            \"questionnaireQuestionId\": 500021,\n" +
            "            \"linearToNum\": 5,\n" +
            "            \"linearLabelStart\": \"Kecewa\",\n" +
            "            \"questionType\": \"LINEAR\",\n" +
            "            \"usedDataValidation\": false,\n" +
            "            \"questionDesc\": \"\",\n" +
            "            \"usedDescription\": false,\n" +
            "            \"questionName\": \"Siapa8\",\n" +
            "            \"linearStartNum\": 1,\n" +
            "            \"shuffleOptionOrder\": false\n" +
            "        },\n" +
            "        {\n" +
            "            \"required\": true,\n" +
            "            \"questionnaireQuestionId\": 500022,\n" +
            "            \"questionType\": \"IMAGE\",\n" +
            "            \"usedDataValidation\": false,\n" +
            "            \"questionDesc\": \"\",\n" +
            "            \"usedDescription\": false,\n" +
            "            \"questionName\": \"Siapa9\",\n" +
            "            \"shuffleOptionOrder\": false\n" +
            "        },\n" +
            "        {\n" +
            "            \"required\": true,\n" +
            "            \"questionnaireQuestionId\": 500023,\n" +
            "            \"questionType\": \"VIDEO\",\n" +
            "            \"usedDataValidation\": false,\n" +
            "            \"questionDesc\": \"\",\n" +
            "            \"usedDescription\": false,\n" +
            "            \"questionName\": \"Siapa10\",\n" +
            "            \"shuffleOptionOrder\": false\n" +
            "        },\n" +
            "        {\n" +
            "            \"required\": true,\n" +
            "            \"questionnaireQuestionId\": 500027,\n" +
            "            \"questionType\": \"REFERENCE\",\n" +
            "            \"referenceValueType\": \"PROVINCE\",\n" +
            "            \"usedDataValidation\": false,\n" +
            "            \"questionDesc\": \"\",\n" +
            "            \"usedDescription\": false,\n" +
            "            \"questionName\": \"Propinsi\",\n" +
            "            \"shuffleOptionOrder\": false,\n" +
            "            \"refChangeTriggerId\": 500026\n" +
            "        },\n" +
            "        {\n" +
            "            \"required\": true,\n" +
            "            \"questionnaireQuestionId\": 500028,\n" +
            "            \"questionType\": \"SECTION\",\n" +
            "            \"referenceValueType\": \"DISTRICT\",\n" +
            "            \"usedDataValidation\": false,\n" +
            "            \"questionDesc\": \"\",\n" +
            "            \"usedDescription\": false,\n" +
            "            \"questionName\": \"District\",\n" +
            "            \"shuffleOptionOrder\": false\n" +
            "        },\n" +
            "        {\n" +
            "            \"required\": true,\n" +
            "            \"questionnaireQuestionId\": 500028,\n" +
            "            \"questionType\": \"REFERENCE\",\n" +
            "            \"referenceValueType\": \"DISTRICT\",\n" +
            "            \"usedDataValidation\": false,\n" +
            "            \"questionDesc\": \"\",\n" +
            "            \"usedDescription\": false,\n" +
            "            \"questionName\": \"District\",\n" +
            "            \"shuffleOptionOrder\": false\n" +
            "        },\n" +
            "        {\n" +
            "            \"required\": true,\n" +
            "            \"questionnaireQuestionId\": 500029,\n" +
            "            \"questionType\": \"CANVAS\",\n" +
            "            \"referenceValueType\": \"DISTRICT\",\n" +
            "            \"usedDataValidation\": false,\n" +
            "            \"questionDesc\": \"\",\n" +
            "            \"usedDescription\": false,\n" +
            "            \"questionName\": \"District\",\n" +
            "            \"shuffleOptionOrder\": false\n" +
            "        }\n" +
            "    ],\n" +
            "    \"responseCode\": \"00\",\n" +
            "    \"responseMessage\": \"Success\"\n" +
            "}";

    public static final String FORM_JSON = "{\n" +
            "    \"entities\": [\n" +
            "        {\n" +
            "            \"required\": true,\n" +
            "            \"questionnaireQuestionId\": 10001,\n" +
            "            \"questionType\": \"SHORT_TEXT\",\n" +
            "            \"usedDataValidation\": true,\n" +
            "            \"usedDescription\": true,\n" +
            "            \"questionName\": \"SIAPA NAMA ANDA\",\n" +
            "            \"shuffleOptionOrder\": false\n" +
            "        },\n" +
            "        {\n" +
            "            \"required\": true,\n" +
            "            \"questionnaireQuestionId\": 10002,\n" +
            "            \"questionType\": \"PARAGRAPH\",\n" +
            "            \"usedDataValidation\": true,\n" +
            "            \"usedDescription\": true,\n" +
            "            \"questionName\": \"DIMANA ALAMAT ANDA\",\n" +
            "            \"shuffleOptionOrder\": false\n" +
            "        },\n" +
            "        {\n" +
            "            \"required\": true,\n" +
            "            \"questionnaireQuestionId\": 10003,\n" +
            "            \"questionType\": \"DROPDOWN\",\n" +
            "            \"usedDataValidation\": true,\n" +
            "            \"usedDescription\": true,\n" +
            "            \"questionName\": \"PENDIDIKAN TERAKHIR\",\n" +
            "            \"shuffleOptionOrder\": false\n" +
            "        },\n" +
            "        {\n" +
            "            \"required\": true,\n" +
            "            \"questionnaireQuestionId\": 10004,\n" +
            "            \"questionType\": \"CHECKBOX\",\n" +
            "            \"usedDataValidation\": true,\n" +
            "            \"usedDescription\": true,\n" +
            "            \"questionName\": \"HOBBY ANDA (>1)\",\n" +
            "            \"shuffleOptionOrder\": false\n" +
            "        },\n" +
            "        {\n" +
            "            \"required\": true,\n" +
            "            \"questionnaireQuestionId\": 10005,\n" +
            "            \"questionType\": \"CHOICE\",\n" +
            "            \"usedDataValidation\": true,\n" +
            "            \"usedDescription\": true,\n" +
            "            \"questionName\": \"JENIS KELAMIN\",\n" +
            "            \"shuffleOptionOrder\": false\n" +
            "        },\n" +
            "        {\n" +
            "            \"required\": true,\n" +
            "            \"questionnaireQuestionId\": 10006,\n" +
            "            \"questionType\": \"SECTION\",\n" +
            "            \"usedDataValidation\": true,\n" +
            "            \"usedDescription\": true,\n" +
            "            \"questionName\": \"SECTION 2\",\n" +
            "            \"shuffleOptionOrder\": false\n" +
            "        },\n" +
            "        {\n" +
            "            \"required\": true,\n" +
            "            \"questionnaireQuestionId\": 10007,\n" +
            "            \"questionType\": \"SHORT_TEXT\",\n" +
            "            \"usedDataValidation\": true,\n" +
            "            \"usedDescription\": true,\n" +
            "            \"questionName\": \"SIAPA NAMA HEWAN PELIHARAAN ANDA\",\n" +
            "            \"shuffleOptionOrder\": false\n" +
            "        },\n" +
            "        {\n" +
            "            \"required\": true,\n" +
            "            \"questionnaireQuestionId\": 10008,\n" +
            "            \"questionType\": \"DROPDOWN\",\n" +
            "            \"usedDataValidation\": true,\n" +
            "            \"usedDescription\": true,\n" +
            "            \"questionName\": \"BAHASA PEMROGRAMAN YANG DISUKAI\",\n" +
            "            \"shuffleOptionOrder\": false\n" +
            "        },\n" +
            "        {\n" +
            "            \"required\": true,\n" +
            "            \"questionnaireQuestionId\": 10009,\n" +
            "            \"questionType\": \"DATE\",\n" +
            "            \"usedDataValidation\": true,\n" +
            "            \"usedDescription\": true,\n" +
            "            \"questionName\": \"TANGGAL LAHIR\",\n" +
            "            \"shuffleOptionOrder\": false\n" +
            "        },\n" +
            "        {\n" +
            "            \"required\": true,\n" +
            "            \"questionnaireQuestionId\": 10010,\n" +
            "            \"questionType\": \"TIME\",\n" +
            "            \"usedDataValidation\": true,\n" +
            "            \"usedDescription\": true,\n" +
            "            \"questionName\": \"JAM BERTEMU\",\n" +
            "            \"shuffleOptionOrder\": false\n" +
            "        },\n" +
            "        {\n" +
            "            \"required\": true,\n" +
            "            \"questionnaireQuestionId\": 10011,\n" +
            "            \"linearToNum\": 5,\n" +
            "            \"linearLabelStart\": \"START\",\n" +
            "            \"questionType\": \"SLIDER\",\n" +
            "            \"usedDataValidation\": true,\n" +
            "            \"usedDescription\": true,\n" +
            "            \"questionName\": \"APA ANDA PUAS BEKERJA DI TEMPAT ANDA\",\n" +
            "            \"linearStartNum\": 1,\n" +
            "            \"shuffleOptionOrder\": false,\n" +
            "            \"linearLabelTo\": \"END\"\n" +
            "        }," +
            "        {\n" +
            "            \"required\": true,\n" +
            "            \"questionnaireQuestionId\": 500021,\n" +
            "            \"linearToNum\": 5,\n" +
            "            \"linearLabelStart\": \"Kecewa\",\n" +
            "            \"questionType\": \"LINEAR\",\n" +
            "            \"usedDataValidation\": false,\n" +
            "            \"questionDesc\": \"\",\n" +
            "            \"usedDescription\": false,\n" +
            "            \"questionName\": \"Siapa8\",\n" +
            "            \"linearStartNum\": 1,\n" +
            "            \"shuffleOptionOrder\": false\n" +
            "        },\n" +
            "        {\n" +
            "            \"required\": true,\n" +
            "            \"questionnaireQuestionId\": 204,\n" +
            "            \"questionType\": \"VIDEO\",\n" +
            "            \"usedDataValidation\": false,\n" +
            "            \"usedDescription\": false,\n" +
            "            \"questionName\": \"Upload video kegiatan\",\n" +
            "            \"shuffleOptionOrder\": false\n" +
            "        },\n" +
            "        {\n" +
            "            \"required\": true,\n" +
            "            \"questionnaireQuestionId\": 205,\n" +
            "            \"questionType\": \"IMAGE\",\n" +
            "            \"usedDataValidation\": false,\n" +
            "            \"usedDescription\": false,\n" +
            "            \"questionName\": \"Upload gambar kegiatan\",\n" +
            "            \"shuffleOptionOrder\": false\n" +
            "        }," +
            "        {\n" +
            "            \"required\": false,\n" +
            "            \"questionnaireQuestionId\": 150112,\n" +
            "            \"linearToNum\": 5,\n" +
            "            \"linearLabelStart\": \"AWAL\",\n" +
            "            \"questionType\": \"REFERENCE\",\n" +
            "            \"referenceValueType\": \"PROVINCE\",\n" +
            "            \"usedDataValidation\": false,\n" +
            "            \"usedDescription\": false,\n" +
            "            \"questionName\": \"PROVINCE\",\n" +
            "            \"linearStartNum\": 1,\n" +
            "            \"shuffleOptionOrder\": false\n" +
            "        },\n" +
            "        {\n" +
            "            \"required\": false,\n" +
            "            \"questionnaireQuestionId\": 150113,\n" +
            "            \"linearToNum\": 5,\n" +
            "            \"linearLabelStart\": \"AWAL\",\n" +
            "            \"questionType\": \"REFERENCE\",\n" +
            "            \"referenceValueType\": \"DISTRICT\",\n" +
            "            \"usedDataValidation\": false,\n" +
            "            \"usedDescription\": false,\n" +
            "            \"questionName\": \"DISTRICT\",\n" +
            "            \"linearStartNum\": 1,\n" +
            "            \"shuffleOptionOrder\": false,\n" +
            "            \"refChangeTriggerId\": 150112\n" +
            "        },\n" +
            "        {\n" +
            "            \"required\": false,\n" +
            "            \"questionnaireQuestionId\": 150114,\n" +
            "            \"linearToNum\": 5,\n" +
            "            \"linearLabelStart\": \"AWAL\",\n" +
            "            \"questionType\": \"REFERENCE\",\n" +
            "            \"referenceValueType\": \"SUBDISTRICT\",\n" +
            "            \"usedDataValidation\": false,\n" +
            "            \"usedDescription\": false,\n" +
            "            \"questionName\": \"SUBDISTRICT\",\n" +
            "            \"linearStartNum\": 1,\n" +
            "            \"shuffleOptionOrder\": false,\n" +
            "            \"refChangeTriggerId\": 150113\n" +
            "        },\n" +
            "        {\n" +
            "            \"required\": false,\n" +
            "            \"questionnaireQuestionId\": 150115,\n" +
            "            \"linearToNum\": 5,\n" +
            "            \"linearLabelStart\": \"AWAL\",\n" +
            "            \"questionType\": \"REFERENCE\",\n" +
            "            \"referenceValueType\": \"VILLAGE\",\n" +
            "            \"usedDataValidation\": false,\n" +
            "            \"usedDescription\": false,\n" +
            "            \"questionName\": \"VILLAGE\",\n" +
            "            \"linearStartNum\": 1,\n" +
            "            \"shuffleOptionOrder\": false,\n" +
            "            \"refChangeTriggerId\": 150114\n" +
            "        }" +
            "    ],\n" +
            "    \"responseCode\": \"00\",\n" +
            "    \"responseMessage\": \"Success\"\n" +
            "}";

    public static final String QDETAIL = "{\n" +
            "    \"entities\": [\n" +
            "        {\n" +
            "            \"mQuestionnaireQuestionDtl\": 100081,\n" +
            "            \"columnText\": \"JAVA\",\n" +
            "            \"labelText\": \"JAVA\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"mQuestionnaireQuestionDtl\": 100083,\n" +
            "            \"columnText\": \"C#\",\n" +
            "            \"labelText\": \"C#\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"mQuestionnaireQuestionDtl\": 100082,\n" +
            "            \"columnText\": \"PHP\",\n" +
            "            \"labelText\": \"PHP\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"mQuestionnaireQuestionDtl\": 100084,\n" +
            "            \"columnText\": \"VB.NET\",\n" +
            "            \"labelText\": \"VB.NET\"\n" +
            "        }\n" +
            "    ],\n" +
            "    \"responseCode\": \"00\",\n" +
            "    \"responseMessage\": \"Success\"\n" +
            "}";

    public static  final String CHECKBOX_HOBI_JSON = "\n{" +
            "    \"entities\": [\n" +
            "        {\n" +
            "            \"mQuestionnaireQuestionDtl\": 100041,\n" +
            "            \"columnText\": \"SEPAK BOLA\",\n" +
            "            \"labelText\": \"SEPAK BOLA\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"mQuestionnaireQuestionDtl\": 100043,\n" +
            "            \"columnText\": \"MAIN GAME\",\n" +
            "            \"labelText\": \"MAIN GAME\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"mQuestionnaireQuestionDtl\": 100042,\n" +
            "            \"columnText\": \"MAIN DOTA\",\n" +
            "            \"labelText\": \"MAIN DOTA\"\n" +
            "        }\n" +
            "    ],\n" +
            "    \"responseCode\": \"00\",\n" +
            "    \"responseMessage\": \"Success\"\n" +
            "}";

    public static final String DPENDIDIKAN = "{\n" +
            "    \"entities\": [\n" +
            "        {\n" +
            "            \"mQuestionnaireQuestionDtl\": 100031,\n" +
            "            \"columnText\": \"SMP\",\n" +
            "            \"labelText\": \"SMP\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"mQuestionnaireQuestionDtl\": 100033,\n" +
            "            \"columnText\": \"D3\",\n" +
            "            \"labelText\": \"D3\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"mQuestionnaireQuestionDtl\": 100032,\n" +
            "            \"columnText\": \"SMU\",\n" +
            "            \"labelText\": \"SMU\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"mQuestionnaireQuestionDtl\": 100034,\n" +
            "            \"columnText\": \"S1\",\n" +
            "            \"labelText\": \"S1\"\n" +
            "        }\n" +
            "    ],\n" +
            "    \"responseCode\": \"00\",\n" +
            "    \"responseMessage\": \"Success\"\n" +
            "}";

    public static final String JNS_KEL_JSON = "{\n" +
            "    \"entities\": [\n" +
            "        {\n" +
            "            \"mQuestionnaireQuestionDtl\": 100051,\n" +
            "            \"columnText\": \"PRIA\",\n" +
            "            \"labelText\": \"PRIA\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"mQuestionnaireQuestionDtl\": 100052,\n" +
            "            \"columnText\": \"WANITA\",\n" +
            "            \"labelText\": \"WANITA\"\n" +
            "        }\n" +
            "    ],\n" +
            "    \"responseCode\": \"00\",\n" +
            "    \"responseMessage\": \"Success\"\n" +
            "}";
    
    private static final String JSON_ASSIGN = "{\n" +
            "    \"responseCode\": \"00\",\n" +
            "    \"list\": [\n" +
            "        {\n" +
            "            \"surveyAssignment\": {\n" +
            "                \"notes\": \"Program Bansos\",\n" +
            "                \"surveyScheduleId\": 301,\n" +
            "                \"respondenTarget\": 33,\n" +
            "                \"surveyAssignmentId\": 202,\n" +
            "                \"assignmentDate\": \"27/12/2016\",\n" +
            "                \"surveyorCode\": \"SURV_CD-2\",\n" +
            "                \"targetDate\": \"30/12/2016\"\n" +
            "            },\n" +
            "            \"countTQuestionnaireAssignment\": 10,\n" +
            "            \"questionnaire\": {\n" +
            "                \"questionnaireId\": 301,\n" +
            "                \"questionnaireName\": \"Program Bansos\",\n" +
            "                \"questionnaireDesc\": \"Survey Program Bansos\"\n" +
            "            },\n" +
            "            \"surveySchedule\": {\n" +
            "                \"surveyScheduleName\": \"Laporan Bansos Nasional 2016\",\n" +
            "                \"surveyScheduleId\": 301,\n" +
            "                \"provinceName\": \"JAKARTA\",\n" +
            "                \"questionnaireId\": 301,\n" +
            "                \"respondenTarget\": 100,\n" +
            "                \"status\": \"A\",\n" +
            "                \"scheduleToDate\": \"30/12/2017\",\n" +
            "                \"provinceCode\": \"DKI\",\n" +
            "                \"surveyProgramCode\": \"BANSOS\",\n" +
            "                \"scheduleFromDate\": \"27/12/2016\"\n" +
            "            }\n" +
            "        },\n" +
            "        {\n" +
            "            \"surveyAssignment\": {\n" +
            "                \"notes\": \"SURVEY ASSIGNMENT RIDWAN\",\n" +
            "                \"surveyScheduleId\": 20002,\n" +
            "                \"respondenTarget\": 20000,\n" +
            "                \"surveyAssignmentId\": 20000,\n" +
            "                \"assignmentDate\": \"10/12/2016\",\n" +
            "                \"surveyorCode\": \"SURV_CD-2\",\n" +
            "                \"targetDate\": \"10/12/2016\"\n" +
            "            },\n" +
            "            \"countTQuestionnaireAssignment\": 4,\n" +
            "            \"questionnaire\": {\n" +
            "                \"questionnaireId\": 9999999,\n" +
            "                \"questionnaireName\": \"QUESTIONNAIRE-999\",\n" +
            "                \"questionnaireDesc\": \"COBADESC\"\n" +
            "            },\n" +
            "            \"surveySchedule\": {\n" +
            "                \"surveyScheduleName\": \"SCHEDULE - 2\",\n" +
            "                \"surveyScheduleId\": 20002,\n" +
            "                \"provinceName\": \"JAKARTA\",\n" +
            "                \"questionnaireId\": 9999999,\n" +
            "                \"respondenTarget\": 4260,\n" +
            "                \"status\": \"A\",\n" +
            "                \"scheduleToDate\": \"10/12/2017\",\n" +
            "                \"provinceCode\": \"DKI\",\n" +
            "                \"surveyProgramCode\": \"SVPR-2\",\n" +
            "                \"scheduleFromDate\": \"09/12/2016\"\n" +
            "            }\n" +
            "        },\n" +
            "        {\n" +
            "            \"surveyAssignment\": {\n" +
            "                \"notes\": \"Penyandang Masalah Kesejahteraan Sosial (PMKS) #1\",\n" +
            "                \"surveyScheduleId\": 302,\n" +
            "                \"respondenTarget\": 33,\n" +
            "                \"surveyAssignmentId\": 205,\n" +
            "                \"assignmentDate\": \"27/12/2016\",\n" +
            "                \"surveyorCode\": \"SURV_CD-2\",\n" +
            "                \"targetDate\": \"30/12/2016\"\n" +
            "            },\n" +
            "            \"countTQuestionnaireAssignment\": 4,\n" +
            "            \"questionnaire\": {\n" +
            "                \"questionnaireId\": 302,\n" +
            "                \"questionnaireName\": \"Penyandang Masalah Kesejahteraan Sosial (PMKS) #1\",\n" +
            "                \"questionnaireDesc\": \"Instrumen Pendataan Penyandang Masalah Kesejahteraan Sosial (PMKS)\"\n" +
            "            },\n" +
            "            \"surveySchedule\": {\n" +
            "                \"surveyScheduleName\": \"Laporan Penyandang Masalah Kesejahteraan Sosial (PMKS) - 2016\",\n" +
            "                \"surveyScheduleId\": 302,\n" +
            "                \"provinceName\": \"JAKARTA\",\n" +
            "                \"questionnaireId\": 302,\n" +
            "                \"respondenTarget\": 100,\n" +
            "                \"status\": \"A\",\n" +
            "                \"scheduleToDate\": \"30/12/2017\",\n" +
            "                \"provinceCode\": \"DKI\",\n" +
            "                \"surveyProgramCode\": \"PMKS\",\n" +
            "                \"scheduleFromDate\": \"27/12/2016\"\n" +
            "            }\n" +
            "        },\n" +
            "        {\n" +
            "            \"surveyAssignment\": {\n" +
            "                \"notes\": \"QUEST-002\",\n" +
            "                \"surveyScheduleId\": 50602,\n" +
            "                \"respondenTarget\": 10,\n" +
            "                \"surveyAssignmentId\": 21604,\n" +
            "                \"assignmentDate\": \"01/03/2017\",\n" +
            "                \"surveyorCode\": \"SURV_CD-2\",\n" +
            "                \"targetDate\": \"31/03/2017\"\n" +
            "            },\n" +
            "            \"countTQuestionnaireAssignment\": 0,\n" +
            "            \"questionnaire\": {\n" +
            "                \"questionnaireId\": 10502,\n" +
            "                \"questionnaireName\": \"QUEST-002\",\n" +
            "                \"questionnaireDesc\": \"QUEST-002 DESC\"\n" +
            "            },\n" +
            "            \"surveySchedule\": {\n" +
            "                \"surveyScheduleName\": \"SCH-002\",\n" +
            "                \"surveyScheduleId\": 50602,\n" +
            "                \"provinceName\": \"JAKARTA\",\n" +
            "                \"questionnaireId\": 10502,\n" +
            "                \"respondenTarget\": 20,\n" +
            "                \"status\": \"A\",\n" +
            "                \"scheduleToDate\": \"31/03/2017\",\n" +
            "                \"provinceCode\": \"DKI\",\n" +
            "                \"surveyProgramCode\": \"PRG-002\",\n" +
            "                \"scheduleFromDate\": \"01/03/2017\"\n" +
            "            }\n" +
            "        },\n" +
            "        {\n" +
            "            \"surveyAssignment\": {\n" +
            "                \"notes\": \"lebih dari 10\",\n" +
            "                \"surveyScheduleId\": 150101,\n" +
            "                \"respondenTarget\": 202934343,\n" +
            "                \"surveyAssignmentId\": 60202,\n" +
            "                \"assignmentDate\": \"01/01/2017\",\n" +
            "                \"surveyorCode\": \"SURV_CD-2\",\n" +
            "                \"targetDate\": \"01/01/2019\"\n" +
            "            },\n" +
            "            \"countTQuestionnaireAssignment\": 1,\n" +
            "            \"questionnaire\": {\n" +
            "                \"questionnaireId\": 30101,\n" +
            "                \"questionnaireName\": \"lebih dari 10\",\n" +
            "                \"questionnaireDesc\": \"TEST LEBIH DARI 10\"\n" +
            "            },\n" +
            "            \"surveySchedule\": {\n" +
            "                \"surveyScheduleName\": \"LEBIH DARI 10\",\n" +
            "                \"surveyScheduleId\": 150101,\n" +
            "                \"provinceName\": \"JAKARTA\",\n" +
            "                \"questionnaireId\": 30101,\n" +
            "                \"respondenTarget\": 405868686,\n" +
            "                \"status\": \"A\",\n" +
            "                \"scheduleToDate\": \"01/01/2019\",\n" +
            "                \"provinceCode\": \"DKI\",\n" +
            "                \"surveyProgramCode\": \"PMKS\",\n" +
            "                \"scheduleFromDate\": \"01/01/2017\"\n" +
            "            }\n" +
            "        },\n" +
            "        {\n" +
            "            \"surveyAssignment\": {\n" +
            "                \"notes\": \"SURV_CD-2 - SVPR-1\",\n" +
            "                \"surveyScheduleId\": 101,\n" +
            "                \"respondenTarget\": 10000,\n" +
            "                \"surveyAssignmentId\": 1,\n" +
            "                \"assignmentDate\": \"15/12/2016\",\n" +
            "                \"surveyorCode\": \"SURV_CD-2\",\n" +
            "                \"targetDate\": \"15/12/2016\"\n" +
            "            },\n" +
            "            \"countTQuestionnaireAssignment\": 4,\n" +
            "            \"questionnaire\": {\n" +
            "                \"questionnaireId\": 101,\n" +
            "                \"questionnaireName\": \"Kepuasan Pelanggan\",\n" +
            "                \"questionnaireDesc\": \"Questionnaire Kepuasan Pelanggan\"\n" +
            "            },\n" +
            "            \"surveySchedule\": {\n" +
            "                \"surveyScheduleName\": \"SVPR-1\",\n" +
            "                \"surveyScheduleId\": 101,\n" +
            "                \"provinceName\": \"JAKARTA\",\n" +
            "                \"questionnaireId\": 101,\n" +
            "                \"respondenTarget\": 10000,\n" +
            "                \"status\": \"A\",\n" +
            "                \"scheduleToDate\": \"15/12/2017\",\n" +
            "                \"provinceCode\": \"DKI\",\n" +
            "                \"surveyProgramCode\": \"SVPR-1\",\n" +
            "                \"scheduleFromDate\": \"15/12/2016\"\n" +
            "            }\n" +
            "        },\n" +
            "        {\n" +
            "            \"surveyAssignment\": {\n" +
            "                \"notes\": \"SURV_CD-2 - SVPR-1\",\n" +
            "                \"surveyScheduleId\": 201,\n" +
            "                \"respondenTarget\": 10000,\n" +
            "                \"surveyAssignmentId\": 101,\n" +
            "                \"assignmentDate\": \"21/12/2016\",\n" +
            "                \"surveyorCode\": \"SURV_CD-2\",\n" +
            "                \"targetDate\": \"21/12/2016\"\n" +
            "            },\n" +
            "            \"countTQuestionnaireAssignment\": 6,\n" +
            "            \"questionnaire\": {\n" +
            "                \"questionnaireId\": 201,\n" +
            "                \"questionnaireName\": \"Program KB Nasional\",\n" +
            "                \"questionnaireDesc\": \"Survey Program KB Nasional\"\n" +
            "            },\n" +
            "            \"surveySchedule\": {\n" +
            "                \"surveyScheduleName\": \"SVPR-1\",\n" +
            "                \"surveyScheduleId\": 201,\n" +
            "                \"provinceName\": \"JAKARTA\",\n" +
            "                \"questionnaireId\": 201,\n" +
            "                \"respondenTarget\": 10000,\n" +
            "                \"status\": \"A\",\n" +
            "                \"scheduleToDate\": \"21/12/2017\",\n" +
            "                \"provinceCode\": \"DKI\",\n" +
            "                \"surveyProgramCode\": \"SVPR-1\",\n" +
            "                \"scheduleFromDate\": \"21/12/2016\"\n" +
            "            }\n" +
            "        },\n" +
            "        {\n" +
            "            \"surveyAssignment\": {\n" +
            "                \"notes\": \"SURV_CD-2 - SVPR-1\",\n" +
            "                \"surveyScheduleId\": 202,\n" +
            "                \"respondenTarget\": 10000,\n" +
            "                \"surveyAssignmentId\": 102,\n" +
            "                \"assignmentDate\": \"21/12/2016\",\n" +
            "                \"surveyorCode\": \"SURV_CD-2\",\n" +
            "                \"targetDate\": \"21/12/2016\"\n" +
            "            },\n" +
            "            \"countTQuestionnaireAssignment\": 3,\n" +
            "            \"questionnaire\": {\n" +
            "                \"questionnaireId\": 202,\n" +
            "                \"questionnaireName\": \"Raskin\",\n" +
            "                \"questionnaireDesc\": \"Kegiatan Raskin\"\n" +
            "            },\n" +
            "            \"surveySchedule\": {\n" +
            "                \"surveyScheduleName\": \"SVPR-1\",\n" +
            "                \"surveyScheduleId\": 202,\n" +
            "                \"provinceName\": \"JAKARTA\",\n" +
            "                \"questionnaireId\": 202,\n" +
            "                \"respondenTarget\": 10000,\n" +
            "                \"status\": \"A\",\n" +
            "                \"scheduleToDate\": \"21/12/2017\",\n" +
            "                \"provinceCode\": \"DKI\",\n" +
            "                \"surveyProgramCode\": \"SVPR-1\",\n" +
            "                \"scheduleFromDate\": \"21/12/2016\"\n" +
            "            }\n" +
            "        },\n" +
            "        {\n" +
            "            \"surveyAssignment\": {\n" +
            "                \"notes\": \"Penyandang Masalah Kesejahteraan Sosial (PMKS) #2\",\n" +
            "                \"surveyScheduleId\": 303,\n" +
            "                \"respondenTarget\": 23,\n" +
            "                \"surveyAssignmentId\": 208,\n" +
            "                \"assignmentDate\": \"27/12/2016\",\n" +
            "                \"surveyorCode\": \"SURV_CD-2\",\n" +
            "                \"targetDate\": \"30/12/2016\"\n" +
            "            },\n" +
            "            \"countTQuestionnaireAssignment\": 9,\n" +
            "            \"questionnaire\": {\n" +
            "                \"questionnaireId\": 303,\n" +
            "                \"questionnaireName\": \"Penyandang Masalah Kesejahteraan Sosial (PMKS) #2\",\n" +
            "                \"questionnaireDesc\": \"Instrumen Pendataan Penyandang Masalah Kesejahteraan Sosial (PMKS)\"\n" +
            "            },\n" +
            "            \"surveySchedule\": {\n" +
            "                \"surveyScheduleName\": \"Laporan Penyandang Masalah Kesejahteraan Sosial (PMKS) - 2016 #2\",\n" +
            "                \"surveyScheduleId\": 303,\n" +
            "                \"provinceName\": \"JAKARTA\",\n" +
            "                \"questionnaireId\": 303,\n" +
            "                \"respondenTarget\": 100,\n" +
            "                \"status\": \"A\",\n" +
            "                \"scheduleToDate\": \"30/12/2017\",\n" +
            "                \"provinceCode\": \"DKI\",\n" +
            "                \"surveyProgramCode\": \"PMKS\",\n" +
            "                \"scheduleFromDate\": \"27/12/2016\"\n" +
            "            }\n" +
            "        },\n" +
            "        {\n" +
            "            \"surveyAssignment\": {\n" +
            "                \"notes\": \"Question Prototype 1 Update\",\n" +
            "                \"surveyScheduleId\": 150001,\n" +
            "                \"respondenTarget\": 4624623629,\n" +
            "                \"surveyAssignmentId\": 60001,\n" +
            "                \"assignmentDate\": \"01/01/2017\",\n" +
            "                \"surveyorCode\": \"SURV_CD-2\",\n" +
            "                \"targetDate\": \"01/01/2019\"\n" +
            "            },\n" +
            "            \"countTQuestionnaireAssignment\": 1,\n" +
            "            \"questionnaire\": {\n" +
            "                \"questionnaireId\": 10101,\n" +
            "                \"questionnaireName\": \"Question Prototype 1 Update\",\n" +
            "                \"questionnaireDesc\": \"Question Prototype 1 Desc Update\"\n" +
            "            },\n" +
            "            \"surveySchedule\": {\n" +
            "                \"surveyScheduleName\": \"NUGIE_TEST\",\n" +
            "                \"surveyScheduleId\": 150001,\n" +
            "                \"provinceName\": \"JAKARTA\",\n" +
            "                \"questionnaireId\": 10101,\n" +
            "                \"respondenTarget\": 4624623629,\n" +
            "                \"status\": \"A\",\n" +
            "                \"scheduleToDate\": \"01/01/2019\",\n" +
            "                \"provinceCode\": \"DKI\",\n" +
            "                \"surveyProgramCode\": \"PMKS\",\n" +
            "                \"scheduleFromDate\": \"01/01/2017\"\n" +
            "            }\n" +
            "        },\n" +
            "        {\n" +
            "            \"surveyAssignment\": {\n" +
            "                \"notes\": \"Penyandang Masalah Kesejahteraan Sosial (PMKS) #3\",\n" +
            "                \"surveyScheduleId\": 300001,\n" +
            "                \"respondenTarget\": 0,\n" +
            "                \"surveyAssignmentId\": 120002,\n" +
            "                \"assignmentDate\": \"23/07/2017\",\n" +
            "                \"surveyorCode\": \"SURV_CD-2\",\n" +
            "                \"targetDate\": \"25/07/2017\"\n" +
            "            },\n" +
            "            \"countTQuestionnaireAssignment\": 0,\n" +
            "            \"questionnaire\": {\n" +
            "                \"questionnaireId\": 20001,\n" +
            "                \"questionnaireName\": \"Penyandang Masalah Kesejahteraan Sosial (PMKS) #3\",\n" +
            "                \"questionnaireDesc\": \"Penyandang Masalah Kesejahteraan Sosial (PMKS) #3\"\n" +
            "            },\n" +
            "            \"surveySchedule\": {\n" +
            "                \"surveyScheduleName\": \"templatetest1\",\n" +
            "                \"surveyScheduleId\": 300001,\n" +
            "                \"provinceName\": \"JAKARTA\",\n" +
            "                \"questionnaireId\": 20001,\n" +
            "                \"respondenTarget\": 1,\n" +
            "                \"status\": \"A\",\n" +
            "                \"scheduleToDate\": \"25/07/2017\",\n" +
            "                \"provinceCode\": \"DKI\",\n" +
            "                \"surveyProgramCode\": \"PMKS\",\n" +
            "                \"scheduleFromDate\": \"23/07/2017\"\n" +
            "            }\n" +
            "        },\n" +
            "        {\n" +
            "            \"surveyAssignment\": {\n" +
            "                \"notes\": \"Penyandang Masalah Kesejahteraan Sosial (PMKS) #3\",\n" +
            "                \"surveyScheduleId\": 600001,\n" +
            "                \"respondenTarget\": 100,\n" +
            "                \"surveyAssignmentId\": 240002,\n" +
            "                \"assignmentDate\": \"20/11/2017\",\n" +
            "                \"surveyorCode\": \"SURV_CD-2\",\n" +
            "                \"targetDate\": \"22/11/2017\"\n" +
            "            },\n" +
            "            \"countTQuestionnaireAssignment\": 0,\n" +
            "            \"questionnaire\": {\n" +
            "                \"questionnaireId\": 90001,\n" +
            "                \"questionnaireName\": \"test20171121\",\n" +
            "                \"questionnaireDesc\": \"test 20171121\"\n" +
            "            },\n" +
            "            \"surveySchedule\": {\n" +
            "                \"surveyScheduleName\": \"Test20171121\",\n" +
            "                \"surveyScheduleId\": 600001,\n" +
            "                \"provinceName\": \"JAKARTA\",\n" +
            "                \"questionnaireId\": 90001,\n" +
            "                \"respondenTarget\": 100,\n" +
            "                \"status\": \"A\",\n" +
            "                \"scheduleToDate\": \"22/11/2017\",\n" +
            "                \"provinceCode\": \"DKI\",\n" +
            "                \"surveyProgramCode\": \"PMKS\",\n" +
            "                \"scheduleFromDate\": \"20/11/2017\"\n" +
            "            }\n" +
            "        }\n" +
            "    ],\n" +
            "    \"responseMessage\": \"Success\"\n" +
            "}";

    private static final String JSON_MEMBER = "{\n" +
            "    \"entity\": {\n" +
            "        \"psksName\": \"Ricky\",\n" +
            "        \"occupationCode\": \"1\",\n" +
            "        \"participantType\": \"2\",\n" +
            "        \"status\": \"A\",\n" +
            "        \"psksNo\": \"123456\",\n" +
            "        \"villageCode\": \"CIPULIR\",\n" +
            "        \"provinceCode\": \"BABEL\",\n" +
            "        \"psksMemberId\": 2,\n" +
            "        \"isCoordinator\": false,\n" +
            "        \"address\": \"Menteng Atas No. 213\",\n" +
            "        \"districtCode\": \"KAB.LIPUKO\",\n" +
            "        \"psksType\": \"PSM\",\n" +
            "        \"gender\": \"L\",\n" +
            "        \"photoId\": 651810,\n" +
            "        \"phoneNumber\": \"087876666779\",\n" +
            "        \"subDistrictCode\": \"TIROANG\",\n" +
            "        \"lastEducationCode\": \"1\"\n" +
            "    },\n" +
            "    \"responseCode\": \"00\",\n" +
            "    \"responseMessage\": \"Success\"\n" +
            "}";
    public static QuestionnaireResponse getQuestioner(String json){
        Gson gson = new Gson();
          return gson.fromJson(json,  QuestionnaireResponse.class);
    }

    public static QuestionDetailResponse getQuestionDetail(String json){
        Gson gson = new Gson();
        return gson.fromJson(json,  QuestionDetailResponse.class);
    }

    public static SurveyAssignmentResponse getSurveyList(){
        Gson gson = new Gson();
        return gson.fromJson(JSON_ASSIGN,  SurveyAssignmentResponse.class);
    }

    public static PKSMemberResponse getPKSMember(){
        Gson gson = new Gson();
        return gson.fromJson(JSON_MEMBER,  PKSMemberResponse.class);
    }
}
