package client.android.psks.validate;

import android.view.View;
import android.widget.EditText;

import client.android.psks.R;

/**
 * Created by winzaldi on 12/8/17.
 */

public class TextEmptyValidationOnFocus implements  View.OnFocusChangeListener {
    private EditText text;

    public TextEmptyValidationOnFocus(EditText text) {
        this.text = text;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (!hasFocus) {
            if (text.getText().toString().length() == 0)
                text.setError(v.getContext().getResources().getString(R.string.empty_text));
        }
    }
}
