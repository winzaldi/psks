package client.android.psks.service;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.io.File;

import client.android.psks.BuildConfig;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by winzaldi on 11/15/17.
 */

public class ServiceGenerator {

    private final static HttpLoggingInterceptor loggingInterceptor =
            new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);


    private final static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(BuildConfig.API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create());

    private final static OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor);

    private static Retrofit retrofit = null;

    public static <S> S createService(Class<S> serviceClass, Context mContext) {
        return createService(serviceClass, mContext, null);
    }


    public static <S> S createService(Class<S> serviceClass, Context mContext, final String authToken) {


        if (!TextUtils.isEmpty(authToken)) {
            AuthenticationInterceptor interceptor
                    = new AuthenticationInterceptor(authToken, mContext);

            if (!httpClient.interceptors().contains(interceptor)) {
                httpClient.addInterceptor(interceptor);
            }
        }

        builder.client(httpClient.build());
        retrofit = builder.build();

        return retrofit.create(serviceClass);
    }

    @NonNull
    public static RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                okhttp3.MultipartBody.FORM, descriptionString);
    }

    @NonNull
    public static MultipartBody.Part createPartFile(String mediaType, String key, @NonNull String strPath) {
        File file = new File(strPath);

        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(mediaType), file);

        return MultipartBody.Part.createFormData(key, file.getName(), requestFile);
    }


}
