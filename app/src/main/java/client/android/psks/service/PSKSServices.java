package client.android.psks.service;

import java.util.List;
import java.util.Map;

import client.android.psks.model.district.DistrictResponse;
import client.android.psks.model.image.ImageResponse;
import client.android.psks.model.lembaga.KelembagaanResponse;
import client.android.psks.model.login.LoginResponse;
import client.android.psks.model.member.PKSMemberResponse;
import client.android.psks.model.parameter.ParameterGroupResponse;
import client.android.psks.model.province.ProvinceResponse;
import client.android.psks.model.question.QuestionDetailResponse;
import client.android.psks.model.quitioner.QuestionnaireResponse;
import client.android.psks.model.response.BaseResponse;
import client.android.psks.model.subdistrict.SubDistrictResponse;
import client.android.psks.model.survey.SurveyAssignmentResponse;
import client.android.psks.model.village.VillageResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

/**
 * Created by winzaldi on 11/15/17.
 */

public interface PSKSServices {
    @FormUrlEncoded
    @POST("rest/user/mobilelogin")
    Call<LoginResponse> login(@Field("username") String username,
                              @Field("password") String password,
                              @Field("imei") String imei);


    @FormUrlEncoded
    @POST("rest/surveyassignment/getListSurveyAssignment")
    Call<SurveyAssignmentResponse> getListSurveyAssignment(@Field("userId") String userId);


    @FormUrlEncoded
    @POST("rest/questionnaire/getQuestionnaireQuestion")
    Call<QuestionnaireResponse> getQuestionnaireQuestion(
            @Field("surveyAssignmentId") String surveyAssignmentId);


    @FormUrlEncoded
    @POST("rest/questionnaire/getQuestionnaireQuestionDtl")
    Call<QuestionDetailResponse> getQuestionnaireQuestionDtl(
            @Field("questionnaireQuestionId") String questionnaireQuestionId);


    @Multipart
    @POST("rest/formdata/saveDataQuestionnaire")
    Call<BaseResponse> saveDataQuestionnaire(@PartMap Map<String, RequestBody> inputs, @Part List<MultipartBody.Part> files);


    @FormUrlEncoded
    @POST("rest/user/changePassword")
    Call<BaseResponse> changePassword(@Field("userId") String userId,
                                      @Field("oldPassword") String oldPassword,
                                      @Field("newPassword") String newPassword);


    @FormUrlEncoded
    @POST("rest/psksmember/getPsksMemberDetails")
    Call<PKSMemberResponse> getPsksMemberDetails(@Field("psksMemberId") String psksMemberId);


    @FormUrlEncoded
    @POST("rest/surveyor/modifySurveyor")
    Call<BaseResponse> modifySurveyor(@Field("surveyorCode") String surveyorCode,
                                      @Field("surveyorName") String surveyorName,
                                      @Field("email") String email,
                                      @Field("phoneNumber") String phoneNumber,
                                      @Field("gender") String gender,
                                      @Field("imeiNo") String imeiNo);

    @FormUrlEncoded
    @POST("rest/psksmember/modifyPsksMember")
    Call<BaseResponse> modifyPsksMember(@Field("psksMemberId") String psksMemberId,
                                        @Field("psksType") String psksType,
                                        @Field("psksNo") String psksNo,
                                        @Field("psksName") String psksName,
                                        @Field("provinceCode") String provinceCode,
                                        @Field("districtCode") String districtCode,
                                        @Field("subDistrictCode") String subDistrictCode,
                                        @Field("villageCode") String villageCode,
                                        @Field("isCoordinator") String isCoordinator,
                                        @Field("address") String address,
                                        @Field("photoId") String photoId,
                                        @Field("phoneNumber") String phoneNumber,
                                        @Field("participantType") String participantType);

    @FormUrlEncoded
    @POST("rest/psksmemberext/modifyPsksMember")
    Call<BaseResponse> modifyPsksMemberEncode(@Field("psksMemberId") String psksMemberId,
                                              @Field("psksType") String psksType,
                                              @Field("psksNo") String psksNo,
                                              @Field("psksName") String psksName,
                                              @Field("provinceCode") String provinceCode,
                                              @Field("districtCode") String districtCode,
                                              @Field("subDistrictCode") String subDistrictCode,
                                              @Field("villageCode") String villageCode,
                                              @Field("isCoordinator") int isCoordinator,
                                              @Field("address") String address,
                                              @Field("photoId") String photoId,
                                              @Field("phoneNumber") String phoneNumber,
                                              @Field("participantType") String participantType,
                                              @Field("file") String file);


    @FormUrlEncoded
    @POST("rest/file/getBinaryData")
    Call<ImageResponse> getBinaryData(@Field("id") String fileId);

    @Multipart
    @POST("rest/file/addFile")
    Call<BaseResponse> uploadFile(@Part MultipartBody.Part file);

    @POST("rest/province/getAll")
    Call<ProvinceResponse> getAllProvince();

    @FormUrlEncoded
    @POST("rest/province/getDistrictsByProvinceCode")
    Call<DistrictResponse> getDistictByProvince(@Field("provinceCode") String provinceCode);

    @POST("rest/district/getAll")
    Call<DistrictResponse> getAllDistict();


    @FormUrlEncoded
    @POST("rest/district/getSubDistrictsByDistrictCode")
    Call<SubDistrictResponse> getSubDistictByDistrictCode(@Field("districtCode") String districtCode);


    @POST("rest/subdistrict/getAll")
    Call<SubDistrictResponse> getSubDistrict();

    @FormUrlEncoded
    @POST("rest/subdistrict/getVillageBySubDistrictCode")
    Call<VillageResponse> getVillageBySubDistrictCode(@Field("subDistrictCode") String subDistrictCode);

    @POST("rest/village/getAll")
    Call<VillageResponse> getAllVillage();


    @FormUrlEncoded
    @POST("rest/parametergroup/getParameterGroupValueByGroupCode")
    Call<ParameterGroupResponse> getParticipantTypeList(@Field("groupCode") String participantType);


    @FormUrlEncoded
    @POST("rest/parametergroup/getParameterGroupValueByGroupCode")
    Call<ParameterGroupResponse> getPsksType(@Field("groupCode") String psksType);


    @FormUrlEncoded
    @POST("rest/kelembagaan/getKelembagaanDetails")
    Call<KelembagaanResponse> getKelembagaanDetails(@Field("idLembaga") String idLembaga);


    @FormUrlEncoded
    @POST("rest/kelembagaan/modifyKelembagaan")
    Call<KelembagaanResponse> updateKelembagaan(@Field("idLembaga") String idLembaga,
                                                @Field("namaLembaga") String namaLembaga,
                                                @Field("alamatLembaga") String alamatLembaga,
                                                @Field("provinceCode") String provinceCode,
                                                @Field("districtCode") String districtCode,
                                                @Field("subDistrictCode") String subDistrictCode,
                                                @Field("villageCode") String villageCode,
                                                @Field("psksMemberId") int psksMemberId,
                                                @Field("ketua") String ketua,
                                                @Field("wakilKetua") String wakilKetua,
                                                @Field("sekretaris") String sekretaris,
                                                @Field("wakilSekretaris") String wakilSekretaris,
                                                @Field("bendahara") String bendahara,
                                                @Field("wakilBendahara") String wakilBendahara,
                                                @Field("sieOlahraga") String sieOlahraga,
                                                @Field("sieHumas") String sieHumas,
                                                @Field("sieSeniBudaya") String sieSeniBudaya,
                                                @Field("siePerlengkapanUmum") String siePerlengkapanUmum,
                                                @Field("sieKerohanian") String sieKerohanian,
                                                @Field("sieKomunikasiPublikasi") String sieKomunikasiPublikasi);


}
