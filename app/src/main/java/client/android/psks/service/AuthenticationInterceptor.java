package client.android.psks.service;

import android.content.Context;
import android.util.Log;

import java.io.IOException;

import client.android.psks.model.response.AccessToken;
import client.android.psks.utils.PSKSConstanta;
import client.android.psks.utils.SharedPrefsUtils;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;

import static client.android.psks.BuildConfig.CLIENTID;
import static client.android.psks.BuildConfig.GRANT_TYPE;
import static client.android.psks.BuildConfig.SECRET_KEY;
import static client.android.psks.BuildConfig.TOKEN_PASS;
import static client.android.psks.BuildConfig.TOKEN_USER;

/**
 * Created by winzaldi on 11/15/17.
 */

public class AuthenticationInterceptor implements Interceptor {

    private String authToken;
    private Context mContext;
    private static final String TAG = AuthenticationInterceptor.class.getName();


    public AuthenticationInterceptor(String token, Context c) {
        this.authToken = token;
        this.mContext = c;
    }


    @Override
    public Response intercept(final Chain chain) throws IOException {

        final Request original = chain.request();
        Request.Builder builder = original.newBuilder()
                .header("Authorization", "Bearer" + authToken);
        Request request = builder.build();

        Response response = chain.proceed(request);
        if (response.code() == 500) {
            //Refresh Token
            PSKSTokenService services = ServiceGenerator.createService(PSKSTokenService.class, mContext);
            Call<AccessToken> call = services.getAccessToken(GRANT_TYPE, CLIENTID, SECRET_KEY, TOKEN_USER, TOKEN_PASS);
            try {
                retrofit2.Response<AccessToken> tokenResponse = call.execute();
                if (tokenResponse.code() == 200) {
                    AccessToken accessToken = tokenResponse.body();
                    SharedPrefsUtils.saveToPrefs(mContext, PSKSConstanta.NEKOT_APP, accessToken.getAccessToken());
                    Request.Builder newbuilder = original.newBuilder()
                            .header("Authorization", "Bearer" + accessToken.getAccessToken());
                    Request newRequest = newbuilder.build();
                    return chain.proceed(newRequest);
                }

            } catch (IOException e) {
                Log.e(TAG, e.getLocalizedMessage());

            }


        }

        return response;
    }
}
