package client.android.psks.ui.survey;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ProgressBar;

import client.android.psks.R;
import client.android.psks.model.survey.SurveyAssignmentResponse;
import client.android.psks.utils.FakeUtils;

public class AssignmentListSurvey extends AppCompatActivity {

    private RecyclerView recyclerView;
    private AssigmentListAdapater adapater;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assignment_list_survey);

        SurveyAssignmentResponse response = FakeUtils.getSurveyList();
        recyclerView = findViewById(R.id.recycleview_list);
        recyclerView.setLayoutManager( new LinearLayoutManager(this));
        adapater = new AssigmentListAdapater(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapater);
        adapater.setListOfSurvey(response);

    }
}
