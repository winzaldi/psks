package client.android.psks.ui.kelembagaan;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.Arrays;
import java.util.List;

import client.android.psks.MainActivity;
import client.android.psks.R;
import client.android.psks.model.district.DistrictAdapter;
import client.android.psks.model.district.DistrictResponse;
import client.android.psks.model.lembaga.KelembagaanResponse;
import client.android.psks.model.login.LoginResponse;
import client.android.psks.model.province.ProvinceAdapter;
import client.android.psks.model.province.ProvinceResponse;
import client.android.psks.model.subdistrict.SubDistrictResponse;
import client.android.psks.model.subdistrict.SubdistrictAdapter;
import client.android.psks.model.village.Entities;
import client.android.psks.model.village.VillageAdapter;
import client.android.psks.model.village.VillageResponse;
import client.android.psks.service.PSKSServices;
import client.android.psks.service.ServiceGenerator;
import client.android.psks.utils.PSKSConstanta;
import client.android.psks.utils.SharedPrefsUtils;
import client.android.psks.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by winzaldi on 1/21/18.
 */

public class FragmentKelembagaan extends Fragment implements MainActivity.OnBackPressedListener {
    private static final String TAG = FragmentKelembagaan.class.getSimpleName();
    private EditText et_nama_lembaga, et_alamat_lembaga, et_ketua, et_wakil_ketua,
            et_sekretaris, et_wakil_sekretaris, et_bendahara, et_wakil_bendahara, et_sie_olahraga, et_sie_humas,
            et_sie_seni_budaya, et_sie_perlengkapan_umum, et_sie_kerohanian, et_sie_komu_publikasi;
    private SearchableSpinner sp_province_code, sp_district_code, sp_subdistrict_code, sp_village_code;
    private CoordinatorLayout coordinatorLayout;
    private Button btn_update;
    private PSKSServices services = null;
    private LoginResponse user;
    private ProgressBar progressBar;
    public static final String MSG_FAILED = "Gagal loading data ";

    public static FragmentKelembagaan newInstance() {
        return new FragmentKelembagaan();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void doBack() {
        MenuItem item = ((MainActivity) getActivity())
                .navigationView.getMenu().getItem(0);
        item.setChecked(true);
        ((MainActivity) getActivity()).onNavigationItemSelected(item);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        user = new Gson().fromJson(SharedPrefsUtils.getFromPrefs(getActivity(), PSKSConstanta.LOGIN_OBJ, ""), LoginResponse.class);
        Log.i(TAG, user.toString());
        View view = inflater.inflate(R.layout.fragment_kelembagaan, container, false);
        ((MainActivity) getActivity()).setOnBackPressedListener(this);
        progressBar = view.findViewById(R.id.progressBar);
        coordinatorLayout = view.findViewById(R.id.coordinator);
        String token = SharedPrefsUtils.getFromPrefs(getActivity(), PSKSConstanta.NEKOT_APP, "");
        services = ServiceGenerator.createService(PSKSServices.class, getActivity(),
                token);

        btn_update = view.findViewById(R.id.btn_update);
        et_nama_lembaga = view.findViewById(R.id.et_nama_lembaga);
        et_alamat_lembaga = view.findViewById(R.id.et_alamat_lembaga);
        sp_province_code = view.findViewById(R.id.sp_province_code);
        sp_province_code.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, final View view, int position, long id) {
                if (sp_district_code != null) {
                    client.android.psks.model.province.Entities entity = (client.android.psks.model.province.Entities) sp_province_code.getSelectedItem();
                    progressBar.setVisibility(View.VISIBLE);
                    Call<DistrictResponse> calldistrict = services.getDistictByProvince(entity.getProvinceCode());
                    calldistrict.enqueue(new Callback<DistrictResponse>() {
                        @Override
                        public void onResponse(Call<DistrictResponse> call, Response<DistrictResponse> response) {
                            progressBar.setVisibility(View.GONE);
                            DistrictAdapter adapter =
                                    new DistrictAdapter(getContext(), android.R.layout.simple_spinner_item, response.body().getEntities());
                            sp_district_code.setAdapter(adapter);

                        }

                        @Override
                        public void onFailure(Call<DistrictResponse> call, Throwable t) {
                            progressBar.setVisibility(View.GONE);
                            //Log.i(TAG, "Failed load District");
                            Utils.showSnakbarMessage(view, "Gagal load data Kode Kabupaten/ Kota Madya");

                        }
                    });
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sp_district_code = view.findViewById(R.id.sp_district_code);
        sp_district_code.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, final View view, int position, long id) {
                if (sp_subdistrict_code != null) {
                    client.android.psks.model.district.Entities entity = (client.android.psks.model.district.Entities) sp_district_code.getSelectedItem();
                    progressBar.setVisibility(View.VISIBLE);
                    Call<SubDistrictResponse> calldistrict = services.getSubDistictByDistrictCode(entity.getDistrictCode());
                    calldistrict.enqueue(new Callback<SubDistrictResponse>() {
                        @Override
                        public void onResponse(Call<SubDistrictResponse> call, Response<SubDistrictResponse> response) {
                            progressBar.setVisibility(View.GONE);
                            SubdistrictAdapter adapter =
                                    new SubdistrictAdapter(getContext(), android.R.layout.simple_spinner_item, response.body().getEntities());
                            sp_subdistrict_code.setAdapter(adapter);

                        }

                        @Override
                        public void onFailure(Call<SubDistrictResponse> call, Throwable t) {
                            progressBar.setVisibility(View.GONE);
                            //Log.i(TAG, "Failed load District");
                            Utils.showSnakbarMessage(view, "Gagal load data Kode Kecamatan");

                        }
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sp_subdistrict_code = view.findViewById(R.id.sp_subdistrict_code);
        sp_subdistrict_code.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, final View view, int position, long id) {
                if (sp_village_code != null) {
                    client.android.psks.model.subdistrict.Entities entity = (client.android.psks.model.subdistrict.Entities) sp_subdistrict_code.getSelectedItem();
                    progressBar.setVisibility(View.VISIBLE);
                    Call<VillageResponse> calldistrict = services.getVillageBySubDistrictCode(entity.getSubDistrictCode());
                    calldistrict.enqueue(new Callback<VillageResponse>() {
                        @Override
                        public void onResponse(Call<VillageResponse> call, Response<VillageResponse> response) {
                            progressBar.setVisibility(View.GONE);
                            VillageAdapter adapter =
                                    new VillageAdapter(getContext(), android.R.layout.simple_spinner_item, response.body().getEntities());
                            sp_village_code.setAdapter(adapter);

                        }

                        @Override
                        public void onFailure(Call<VillageResponse> call, Throwable t) {
                            progressBar.setVisibility(View.GONE);
                            //Log.i(TAG, "Failed load Village");
                            Utils.showSnakbarMessage(coordinatorLayout, "Gagal load data Kode Kelurahan/Desa");

                        }
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_village_code = view.findViewById(R.id.sp_village_code);

        et_ketua = view.findViewById(R.id.et_ketua);
        et_wakil_ketua = view.findViewById(R.id.et_wakil_ketua);
        et_sekretaris = view.findViewById(R.id.et_sekretaris);
        et_wakil_sekretaris = view.findViewById(R.id.et_wakil_sekretaris);
        et_bendahara = view.findViewById(R.id.et_bendahara);
        et_wakil_bendahara= view.findViewById(R.id.et_wakil_bendahara);
        et_sie_olahraga = view.findViewById(R.id.et_sie_olahraga);
        et_sie_humas = view.findViewById(R.id.et_sie_humas);
        et_sie_seni_budaya = view.findViewById(R.id.et_sie_seni_budaya);
        et_sie_perlengkapan_umum = view.findViewById(R.id.et_sie_perlengkapan_umum);
        et_sie_kerohanian = view.findViewById(R.id.et_sie_kerohanian);
        et_sie_komu_publikasi = view.findViewById(R.id.et_sie_komu_publikasi);

        services = ServiceGenerator.createService(PSKSServices.class, getContext(),
                SharedPrefsUtils.getFromPrefs(getActivity(), PSKSConstanta.NEKOT_APP, ""));

        Call<KelembagaanResponse> callLembaga = services.getKelembagaanDetails(user.getEntity().getIdLembaga());
        callLembaga.enqueue(new Callback<KelembagaanResponse>() {
            @Override
            public void onResponse(Call<KelembagaanResponse> call, Response<KelembagaanResponse> response) {
                KelembagaanResponse res = response.body();
                if (response.code() == 200) {
                    setData(res);
                }

            }

            @Override
            public void onFailure(Call<KelembagaanResponse> call, Throwable t) {
                Utils.showSnakbarMessage(coordinatorLayout, "Data Kelembagaan tidak ditemukan");
            }
        });


        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String namaLembaga = et_nama_lembaga.getText().toString();
                String alamatLembaga = et_alamat_lembaga.getText().toString();

                client.android.psks.model.province.Entities entProv =
                        (client.android.psks.model.province.Entities) sp_province_code.getSelectedItem();
                client.android.psks.model.district.Entities entDistr =
                        (client.android.psks.model.district.Entities) sp_district_code.getSelectedItem();
                client.android.psks.model.subdistrict.Entities entSubDistr =
                        (client.android.psks.model.subdistrict.Entities) sp_subdistrict_code.getSelectedItem();
                String provCode = entProv.getProvinceCode();
                String districtCode = entDistr.getDistrictCode();
                String subDistrictCode = entSubDistr.getSubDistrictCode();
                String villCode = "";
                if (sp_village_code.getSelectedItem() != null) {
                    client.android.psks.model.village.Entities entVillage =
                            (client.android.psks.model.village.Entities) sp_village_code.getSelectedItem();
                    villCode = entVillage.getVillageCode();
                }

                String ketua = et_ketua.getText().toString();
                String wakilKetua = et_wakil_ketua.getText().toString();
                String sekretaris = et_sekretaris.getText().toString();
                String wakilSekretaris = et_wakil_sekretaris.getText().toString();
                String bendahara = et_bendahara.getText().toString();
                String wakilBendahara = et_wakil_bendahara.getText().toString();
                String sieOlahraga = et_sie_olahraga.getText().toString();
                String sieHumas = et_sie_humas.getText().toString();
                String sieSeniBudaya = et_sie_seni_budaya.getText().toString();
                String siePerlengkapanUmum = et_sie_perlengkapan_umum.getText().toString();
                String sieKerohanian = et_sie_kerohanian.getText().toString();
                String sieKomunikasiPublikasi = et_sie_komu_publikasi.getText().toString();

                Call<KelembagaanResponse> rescall = services.updateKelembagaan(user.getEntity().getIdLembaga(),
                        namaLembaga, alamatLembaga, provCode, districtCode, subDistrictCode, villCode,
                        Integer.parseInt(user.getEntity().getPsksMemberId()), ketua,wakilKetua,sekretaris,
                        wakilSekretaris,bendahara,wakilBendahara,sieOlahraga,sieHumas,sieSeniBudaya,
                        siePerlengkapanUmum,sieKerohanian,sieKomunikasiPublikasi);
                progressBar.setVisibility(View.VISIBLE);
                rescall.enqueue(new Callback<KelembagaanResponse>() {
                    @Override
                    public void onResponse(Call<KelembagaanResponse> call, Response<KelembagaanResponse> response) {
                        progressBar.setVisibility(View.GONE);
                        Log.i(TAG, response.message());
                        if (response.code() == 200) {
                            Utils.showSnakbarMessage(coordinatorLayout, "Success to Updated");
                        } else if (response.code() == 408) {
                            Utils.showSnakbarMessage(coordinatorLayout, "Request Time Out");
                        } else {
                            Utils.showSnakbarMessage(coordinatorLayout, "Failed to Updated");
                        }
                    }

                    @Override
                    public void onFailure(Call<KelembagaanResponse> call, Throwable t) {
                        progressBar.setVisibility(View.GONE);
                        Log.i(TAG, t.getLocalizedMessage());
                        Utils.showSnakbarMessage(coordinatorLayout, "Failed to Updated");
                    }
                });

            }
        });

        return view;
    }

    private void setVillageCode(final KelembagaanResponse res) {
        //VillageResponse resdata = FakeReferences.getVillage();

        if (sp_subdistrict_code.getSelectedItem() != null) {
            client.android.psks.model.subdistrict.Entities entity =
                    (client.android.psks.model.subdistrict.Entities) sp_subdistrict_code.getSelectedItem();
            if (entity != null) {

                Call<VillageResponse> callvillage = services.getVillageBySubDistrictCode(entity.getSubDistrictCode());
                callvillage.enqueue(new Callback<VillageResponse>() {
                    @Override
                    public void onResponse(Call<VillageResponse> call, Response<VillageResponse> response) {

                        List<Entities> list = Arrays.asList(response.body().getEntities());
                        ArrayAdapter<Entities> adapter =
                                new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, list);
                        sp_village_code.setAdapter(adapter);


                        int position = 0;
                        if (res.getEntity().getSubDistrictCode() != null) {

                            for (int idx = 0; idx < response.body().getEntities().length; idx++) {
                                client.android.psks.model.village.Entities e = response.body().getEntities()[idx];
                                if (res.getEntity().getVillageCode().equalsIgnoreCase(e.getVillageCode())) {
                                    position = idx;
                                }
                            }
                        }
                        sp_village_code.setSelection(position);

                    }

                    @Override
                    public void onFailure(Call<VillageResponse> call, Throwable t) {
                        //Log.i(TAG, "Faild Load Village ");
                        Utils.showSnakbarMessage(coordinatorLayout, MSG_FAILED + " Desa ");
                    }
                });

            }

        } else {

            Call<VillageResponse> callvillage = services.getAllVillage();
            callvillage.enqueue(new Callback<VillageResponse>() {
                @Override
                public void onResponse(Call<VillageResponse> call, Response<VillageResponse> response) {

                    List<client.android.psks.model.village.Entities> list = Arrays.asList(response.body().getEntities());
                    ArrayAdapter<client.android.psks.model.village.Entities> adapter =
                            new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, list);
                    sp_village_code.setAdapter(adapter);

                    int position = 0;
                    if (res.getEntity().getSubDistrictCode() != null) {

                        for (int idx = 0; idx < response.body().getEntities().length; idx++) {
                            client.android.psks.model.village.Entities e = response.body().getEntities()[idx];
                            if (res.getEntity().getVillageCode().equalsIgnoreCase(e.getVillageCode())) {
                                position = idx;
                            }
                        }
                    }
                    sp_village_code.setSelection(position);

                }

                @Override
                public void onFailure(Call<VillageResponse> call, Throwable t) {
                    //Log.i(TAG, "Faild Load Village ");
                    Utils.showSnakbarMessage(coordinatorLayout, MSG_FAILED + " Desa ");
                }
            });

        }


    }

    private void setSubDistrictCode(final KelembagaanResponse res) {
        //SubDistrictResponse resdata = FakeReferences.getSubDistrict();

        if (sp_district_code.getSelectedItem() != null) {
            client.android.psks.model.district.Entities entitiy =
                    (client.android.psks.model.district.Entities) sp_district_code.getSelectedItem();
            if (entitiy != null) {
                Call<SubDistrictResponse> subDistrictcall = services.getSubDistictByDistrictCode(entitiy.getDistrictCode());
                subDistrictcall.enqueue(new Callback<SubDistrictResponse>() {
                    @Override
                    public void onResponse(Call<SubDistrictResponse> call, Response<SubDistrictResponse> response) {
                        //List<client.android.psks.model.subdistrict.Entities> list = Arrays.asList(response.body().getEntities());
                        SubdistrictAdapter adapter =
                                new SubdistrictAdapter(getContext(), android.R.layout.simple_spinner_item, response.body().getEntities());
                        sp_subdistrict_code.setAdapter(adapter);

                        int position = 0;
                        if (res.getEntity().getSubDistrictCode() != null) {

                            for (int idx = 0; idx < response.body().getEntities().length; idx++) {
                                client.android.psks.model.subdistrict.Entities e = response.body().getEntities()[idx];
                                if (res.getEntity().getSubDistrictCode().equalsIgnoreCase(e.getSubDistrictCode())) {
                                    position = idx;
                                }
                            }
                        }
                        sp_subdistrict_code.setSelection(position);
                    }

                    @Override
                    public void onFailure(Call<SubDistrictResponse> call, Throwable t) {
                        //Log.i(TAG, "Faild Load SubDistrict Data ");
                        Utils.showSnakbarMessage(coordinatorLayout, MSG_FAILED + " Sub District ");

                    }
                });

            }

        } else {

            Call<SubDistrictResponse> subDistrictcall = services.getSubDistrict();
            subDistrictcall.enqueue(new Callback<SubDistrictResponse>() {
                @Override
                public void onResponse(Call<SubDistrictResponse> call, Response<SubDistrictResponse> response) {
                    //List<client.android.psks.model.subdistrict.Entities> list = Arrays.asList(response.body().getEntities());
                    SubdistrictAdapter adapter =
                            new SubdistrictAdapter(getContext(), android.R.layout.simple_spinner_item, response.body().getEntities());
                    sp_subdistrict_code.setAdapter(adapter);

                    int position = 0;
                    if (res.getEntity().getSubDistrictCode() != null) {

                        for (int idx = 0; idx < response.body().getEntities().length; idx++) {
                            client.android.psks.model.subdistrict.Entities e = response.body().getEntities()[idx];
                            if (res.getEntity().getSubDistrictCode().equalsIgnoreCase(e.getSubDistrictCode())) {
                                position = idx;
                            }
                        }
                    }
                    sp_subdistrict_code.setSelection(position);
                }

                @Override
                public void onFailure(Call<SubDistrictResponse> call, Throwable t) {
                    //Log.i(TAG, "Faild Load SubDistrict Data ");
                    Utils.showSnakbarMessage(coordinatorLayout, MSG_FAILED + " Sub District ");
                }
            });

        }


    }

    private void setDistrictCode(final KelembagaanResponse res) {

        //DistrictResponse resdata = FakeReferences.getDistrict();

        if (sp_province_code.getSelectedItem() != null) {
            client.android.psks.model.province.Entities entity =
                    (client.android.psks.model.province.Entities) sp_province_code.getSelectedItem();
            if (entity != null) {

                Call<DistrictResponse> calldistrict = services.getDistictByProvince(entity.getProvinceCode());
                calldistrict.enqueue(new Callback<DistrictResponse>() {
                    @Override
                    public void onResponse(Call<DistrictResponse> call, Response<DistrictResponse> response) {
                        List<client.android.psks.model.district.Entities> list = Arrays.asList(response.body().getEntities());
                        ArrayAdapter<client.android.psks.model.district.Entities> adapter =
                                new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, list);
                        sp_district_code.setAdapter(adapter);

                        int position = 0;
                        if (res.getEntity().getDistrictCode() != null) {

                            for (int idx = 0; idx < response.body().getEntities().length; idx++) {
                                client.android.psks.model.district.Entities e = response.body().getEntities()[idx];
                                if (res.getEntity().getDistrictCode().equalsIgnoreCase(e.getDistrictCode())) {
                                    position = idx;
                                }
                            }
                        }
                        sp_district_code.setSelection(position);
                    }

                    @Override
                    public void onFailure(Call<DistrictResponse> call, Throwable t) {
                        //Log.i(TAG, "Failed load District");
                        Utils.showSnakbarMessage(coordinatorLayout, MSG_FAILED + " Distict ");

                    }
                });

            }


        } else {

            Call<DistrictResponse> calldistrict = services.getAllDistict();
            calldistrict.enqueue(new Callback<DistrictResponse>() {
                @Override
                public void onResponse(Call<DistrictResponse> call, Response<DistrictResponse> response) {
                    List<client.android.psks.model.district.Entities> list = Arrays.asList(response.body().getEntities());
                    ArrayAdapter<client.android.psks.model.district.Entities> adapter =
                            new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, list);
                    sp_district_code.setAdapter(adapter);

                    int position = 0;
                    if (res.getEntity().getDistrictCode() != null) {

                        for (int idx = 0; idx < response.body().getEntities().length; idx++) {
                            client.android.psks.model.district.Entities e = response.body().getEntities()[idx];
                            if (res.getEntity().getDistrictCode().equalsIgnoreCase(e.getDistrictCode())) {
                                position = idx;
                            }
                        }
                    }
                    sp_district_code.setSelection(position);
                }

                @Override
                public void onFailure(Call<DistrictResponse> call, Throwable t) {
                    //Log.i(TAG, "Failed load District");
                    Utils.showSnakbarMessage(coordinatorLayout, MSG_FAILED + " District ");

                }
            });

        }


    }

    private void setProvinceCode(final KelembagaanResponse res) {
        Call<ProvinceResponse> callProv = services.getAllProvince();
        callProv.enqueue(new Callback<ProvinceResponse>() {
            @Override
            public void onResponse(Call<ProvinceResponse> call, Response<ProvinceResponse> response) {
                client.android.psks.model.province.Entities[] list = response.body().getEntities();
                if (list != null) {
                    ProvinceAdapter adapter =
                            new ProvinceAdapter(getContext(), android.R.layout.simple_spinner_item, list);
                    sp_province_code.setAdapter(adapter);

                    int position = 0;
                    if (res.getEntity().getProvinceCode() != null) {

                        for (int idx = 0; idx < response.body().getEntities().length; idx++) {
                            client.android.psks.model.province.Entities e = response.body().getEntities()[idx];
                            if (res.getEntity().getProvinceCode().equalsIgnoreCase(e.getProvinceCode())) {
                                position = idx;
                            }
                        }
                    }
                    sp_province_code.setSelection(position);
                }

            }

            @Override
            public void onFailure(Call<ProvinceResponse> call, Throwable t) {
                //Log.i(TAG, "Failed Load Province");
                Utils.showSnakbarMessage(coordinatorLayout, MSG_FAILED + " Provinsi ");
            }
        });

        services = ServiceGenerator.createService(PSKSServices.class, getContext(),
                SharedPrefsUtils.getFromPrefs(getActivity(), PSKSConstanta.NEKOT_APP, ""));

        //Call<KelembagaanResponse> callLembaga = services.getKelembagaanDetails();


    }

    private void setData(KelembagaanResponse res) {
        et_nama_lembaga.setText(res.getEntity().getNamaLembaga());
        et_alamat_lembaga.setText(res.getEntity().getAlamatLembaga());
        setProvinceCode(res);
        setDistrictCode(res);
        setSubDistrictCode(res);
        setVillageCode(res);
        et_ketua.setText(res.getEntity().getKetua());
        et_wakil_ketua.setText(res.getEntity().getWakilKetua());
        et_sekretaris.setText(res.getEntity().getSekretaris());
        et_wakil_sekretaris.setText(res.getEntity().getWakilSekretaris());
        et_bendahara.setText(res.getEntity().getBendahara());
        et_wakil_bendahara.setText(res.getEntity().getWakilBendahara());
        et_sie_olahraga.setText(res.getEntity().getSieOlahraga());
        et_sie_humas.setText(res.getEntity().getSieHumas());
        et_sie_seni_budaya.setText(res.getEntity().getSieSeniBudaya());
        et_sie_perlengkapan_umum.setText(res.getEntity().getSiePerlengkapanUmum());
        et_sie_kerohanian.setText(res.getEntity().getSieKerohanian());
        et_sie_komu_publikasi.setText(res.getEntity().getSieKomunikasiPublikasi());
    }
}
