package client.android.psks.ui.survey;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import client.android.psks.Form;
import client.android.psks.R;
import client.android.psks.model.survey.SurveyAssignmentResponse;
import client.android.psks.utils.PSKSConstanta;

/**
 * Created by winzaldi on 11/28/17.
 */

public class AssigmentListAdapater extends RecyclerView.Adapter<AssigmentListAdapater.AssigmentListViewHolder> {

    private Context mContext;
    private SurveyAssignmentResponse response;
    private LayoutInflater mInflater;

    public AssigmentListAdapater(Context context) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.response = new SurveyAssignmentResponse();
    }

    @Override
    public AssigmentListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_assignment_list_survey, parent, false);
        final AssigmentListViewHolder viewHolder = new AssigmentListViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AssigmentListViewHolder holder, final int position) {
        holder.getTvTitle().setText(response.getList()[position].getQuestionnaire().getQuestionnaireName());
        holder.getTvDescription().setText(response.getList()[position].getQuestionnaire().getQuestionnaireDesc());
        holder.getTvProvinsi()
                .setText(response.getList()[position].getSurveySchedule().getProvinceCode() + " - "
                        + response.getList()[position].getSurveySchedule().getProvinceName());
        holder.getTvStatus().setText(response.getList()[position].getSurveySchedule().getStatus());
        holder.getTvFromDate().setText(response.getList()[position].getSurveySchedule().getScheduleFromDate());
        holder.getTvToDate().setText(response.getList()[position].getSurveySchedule().getScheduleToDate());

        holder.getCardView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(mContext, "Clicked Card " + position, Toast.LENGTH_SHORT).show();
//                FragmentTransaction fragmentTransaction =((AppCompatActivity)mContext).getSupportFragmentManager().beginTransaction();
//                fragmentTransaction.replace(R.id.frame, FragmentForm.newInstance(),"FragmentForm");
//                fragmentTransaction.commit();
                Intent intent =  new Intent(mContext, Form.class);
                intent.putExtra(PSKSConstanta.SURVEY_ASSIGMENT_ID,response.getList()[position].getSurveyAssignment().getSurveyAssignmentId());
                mContext.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return response.getList().length;
    }

    public void setListOfSurvey(SurveyAssignmentResponse response) {
        this.response = response;
        notifyDataSetChanged();
    }

    class AssigmentListViewHolder extends RecyclerView.ViewHolder {

        private TextView tvTitle,tvDescription,tvProvinsi, tvStatus, tvFromDate, tvToDate;
        private CardView cardView;

        public AssigmentListViewHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.card_view);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvDescription = itemView.findViewById(R.id.tv_title_desc);
            tvProvinsi = itemView.findViewById(R.id.tv_provinsi);
            tvStatus = itemView.findViewById(R.id.tv_status);
            tvFromDate = itemView.findViewById(R.id.tv_from_date);
            tvToDate = itemView.findViewById(R.id.tv_to_date);
        }

        public TextView getTvProvinsi() {
            return tvProvinsi;
        }

        public TextView getTvStatus() {
            return tvStatus;
        }

        public TextView getTvFromDate() {
            return tvFromDate;
        }

        public TextView getTvToDate() {
            return tvToDate;
        }

        public TextView getTvTitle() {
            return tvTitle;
        }

        public TextView getTvDescription() {
            return tvDescription;
        }

        public CardView getCardView() {
            return cardView;
        }
    }
}
