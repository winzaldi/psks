package client.android.psks.ui.form;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import net.alhazmy13.mediapicker.Image.ImagePicker;
import net.alhazmy13.mediapicker.Video.VideoPicker;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import client.android.psks.Form;
import client.android.psks.R;
import client.android.psks.model.district.DistrictAdapter;
import client.android.psks.model.district.DistrictResponse;
import client.android.psks.model.province.ProvinceAdapter;
import client.android.psks.model.province.ProvinceResponse;
import client.android.psks.model.question.QuestionDetailAdapter;
import client.android.psks.model.question.QuestionDetailResponse;
import client.android.psks.model.quitioner.Entities;
import client.android.psks.model.quitioner.QuestionnaireResponse;
import client.android.psks.model.subdistrict.SubDistrictResponse;
import client.android.psks.model.subdistrict.SubdistrictAdapter;
import client.android.psks.model.village.VillageAdapter;
import client.android.psks.model.village.VillageResponse;
import client.android.psks.utils.FakeReferences;
import client.android.psks.utils.FakeUtils;
import client.android.psks.utils.PSKSConstanta;
import client.android.psks.utils.Utils;
import client.android.psks.validate.TextEmptyValidation;

/**
 * Created by winzaldi on 11/29/17.
 */

public class FragmentForm extends Fragment {

    private static final String TAG = Form.class.getName();
    QuestionnaireResponse response;
    private LinearLayout llLayout;
    private LayoutInflater inflater;
    private List<View> container = new ArrayList<>();
    private EditText selectedEditText;
    private Calendar calendar;
    private ImagePicker imagePicker;
    private VideoPicker videoPicker;
    private ImageView selectedImageView;
    private static FragmentForm form = null;

    public static FragmentForm newInstance() {
        if (form != null) return form;
        form = new FragmentForm();
        return form;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup viewGroup, Bundle savedInstanceState) {
        super.onCreateView(inflater, viewGroup, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_form, viewGroup, false);
        llLayout = (LinearLayout) view.findViewById(R.id.llLayout);
        response = FakeUtils.getQuestioner(FakeUtils.FORM_JSON_2);
        calendar = Calendar.getInstance();

        Entities[] entities = response.getEntities();
        for (int i = 0; i < entities.length; i++) {
            Entities entity = entities[i];


            if (entity.getQuestionType().equalsIgnoreCase("SHORT_TEXT")) {
                TextInputLayout textInputLayout = (TextInputLayout) inflater.inflate(R.layout.comp_short_text, null);
                textInputLayout.setHint(entity.getQuestionName());
                final TextInputEditText textInputEditText = (TextInputEditText) textInputLayout.getEditText();
                textInputEditText.setId(Utils.generateViewId());
                //set variable name
                textInputEditText.setTag(entity.getQuestionnaireQuestionId());
                if (entity.getRequired().equalsIgnoreCase("true")) {
                    textInputEditText.addTextChangedListener(new TextEmptyValidation(textInputEditText,getContext()));
                }


                //add child view to parent
                llLayout.addView(textInputLayout);
                container.add(textInputEditText);
            } else if (entity.getQuestionType().equalsIgnoreCase("PARAGRAPH")) {

                TextInputLayout textInputLayout = (TextInputLayout) inflater.inflate(R.layout.comp_paragraph, null);
                textInputLayout.setHint(entity.getQuestionName());
                final TextInputEditText textInputEditText = (TextInputEditText) textInputLayout.getEditText();
                textInputEditText.setId(Utils.generateViewId());
                //set variable name
                textInputEditText.setTag(entity.getQuestionnaireQuestionId());
                if (entity.getRequired().equalsIgnoreCase("true")) {
                    textInputEditText.addTextChangedListener(new TextEmptyValidation(textInputEditText,getContext()));
                }

                llLayout.addView(textInputLayout);
                container.add(textInputEditText);
            } else if (entity.getQuestionType().equalsIgnoreCase("DROPDOWN")) {
                LinearLayout llradio = (LinearLayout) inflater.inflate(R.layout.comp_label_spinner, null);
                TextView textView = (TextView) llradio.getChildAt(0);
                textView.setText(entity.getQuestionName());
                llLayout.addView(llradio);
                llradio.setTag(entity.getQuestionnaireQuestionId());
                //get Data For Spinner
                QuestionDetailResponse detailResponse = FakeUtils.getQuestionDetail(FakeUtils.QDETAIL);
                //create Adapater
                QuestionDetailAdapter dataAdapter = new QuestionDetailAdapter(getActivity(), android.R.layout.simple_spinner_item, detailResponse.getEntities());
                //createSpinner and set Adapater
                Spinner spinner = (Spinner) inflater.inflate(R.layout.comp_spinner, null);
                spinner.setTag(entity.getQuestionnaireQuestionId());
                spinner.setAdapter(dataAdapter);

                llLayout.addView(spinner);
                container.add(spinner);
            } else if (entity.getQuestionType().equalsIgnoreCase("CHECKBOX")) {
                LinearLayout llcheckbox = (LinearLayout) inflater.inflate(R.layout.comp_label_checkbox, null);
                TextView textView = (TextView) llcheckbox.getChildAt(0);
                textView.setText(entity.getQuestionName());
                llLayout.addView(llcheckbox);
                llcheckbox.setTag(entity.getQuestionnaireQuestionId());
                QuestionDetailResponse detailResponse = FakeUtils.getQuestionDetail(FakeUtils.CHECKBOX_HOBI_JSON);
                for (int idx = 0; idx < detailResponse.getEntities().length; idx++) {
                    client.android.psks.model.question.Entities item = detailResponse.getEntities()[idx];
                    CheckBox checkBox = (CheckBox) inflater.inflate(R.layout.comp_checkbox, null);
                    checkBox.setTag(item.getMQuestionnaireQuestionDtl());
                    checkBox.setText(item.getLabelText());
                    llcheckbox.addView(checkBox);
                    container.add(checkBox);
                }

            } else if (entity.getQuestionType().equalsIgnoreCase("CHOICE")) {
//                RadioGroup radioGroup =(RadioGroup) inflater.inflate(R.layout.comp_choice,null);
//                radioGroup.setTag(entity.getQuestionnaireQuestionId());
//                llLayout.addView(radioGroup);
//                container.add(radioGroup);

                LinearLayout lradio = (LinearLayout) inflater.inflate(R.layout.comp_label_radio, null);
                TextView textView = (TextView) lradio.getChildAt(0);
                textView.setText(entity.getQuestionName());
                llLayout.addView(lradio);
                lradio.setTag(entity.getQuestionnaireQuestionId());
                RadioGroup radioGroup = (RadioGroup) lradio.getChildAt(1);

                QuestionDetailResponse detailResponse = FakeUtils.getQuestionDetail(FakeUtils.JNS_KEL_JSON);
                if (detailResponse.getEntities().length > 2) {
                    radioGroup.setOrientation(LinearLayout.VERTICAL);
                } else {
                    radioGroup.setOrientation(LinearLayout.HORIZONTAL);
                }
                for (int idx = 0; idx < detailResponse.getEntities().length; idx++) {
                    client.android.psks.model.question.Entities item = detailResponse.getEntities()[idx];
                    RadioButton radioButton = new RadioButton(getActivity());
                    radioButton.setTag(item.getMQuestionnaireQuestionDtl());
                    radioButton.setId(Utils.generateViewId());
                    radioButton.setText(item.getLabelText());
                    radioGroup.addView(radioButton, idx);
                }
                container.add(radioGroup);
            } else if (entity.getQuestionType().equalsIgnoreCase("SECTION")) {
                Log.i(TAG, "SECTION");
                LinearLayout llsection = (LinearLayout) inflater.inflate(R.layout.comp_section, null);
                llLayout.addView(llsection);
            } else if (entity.getQuestionType().equalsIgnoreCase("DATE")) {
                TextInputLayout textInputLayout = (TextInputLayout) inflater.inflate(R.layout.comp_date, null);
                final TextInputEditText inputEditText = (TextInputEditText) textInputLayout.getEditText();
                inputEditText.setTag(entity.getQuestionnaireQuestionId());
                inputEditText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pickDate(inputEditText, getActivity());
                    }
                });
                llLayout.addView(textInputLayout);
                container.add(inputEditText);

            } else if (entity.getQuestionType().equalsIgnoreCase("TIME")) {
                Log.i(TAG, "TIME");
                TextInputLayout textInputLayout = (TextInputLayout) inflater.inflate(R.layout.comp_time, null);
                final TextInputEditText inputEditText = (TextInputEditText) textInputLayout.getEditText();
                inputEditText.setTag(entity.getQuestionnaireQuestionId());
                inputEditText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pickTime(inputEditText, getActivity());
                    }
                });
                llLayout.addView(textInputLayout);
                container.add(inputEditText);
            } else if (entity.getQuestionType().equalsIgnoreCase("SLIDER")) {
                Log.i(TAG, "RATING");
                LinearLayout llRatingBar = (LinearLayout) inflater.inflate(R.layout.comp_start, null);
                RatingBar ratingBar = (RatingBar) llRatingBar.getChildAt(0);
                ratingBar.setTag(entity.getQuestionnaireQuestionId());
                Log.i(TAG, "RATING" + entity.getLinearToNum());
                ratingBar.setNumStars(Integer.parseInt(entity.getLinearToNum()));
                ratingBar.setRating(Integer.parseInt(entity.getLinearStartNum()));

                llLayout.addView(llRatingBar);
                container.add(ratingBar);


            } else if (entity.getQuestionType().equalsIgnoreCase("LINEAR")) {
                Log.i(TAG, "LINEAR");
                LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.comp_radiogroup_linear, null);
                TextView textView = (TextView) linearLayout.getChildAt(0);
                textView.setText(entity.getQuestionName());
                LinearLayout linearLayout2 = (LinearLayout) linearLayout.getChildAt(1);
                RadioGroup radioGroup = (RadioGroup) linearLayout2.getChildAt(1);
                for (int idx = 0; idx < Integer.parseInt(entity.getLinearToNum()); idx++) {
                    RadioButton radioButton = (RadioButton) inflater.inflate(R.layout.comp_radio_text_ontop, null);
                    int number = idx + 1;
                    radioButton.setText(number + "");
                    radioGroup.addView(radioButton);
                }
                llLayout.addView(linearLayout);
                container.add(radioGroup);
            } else if (entity.getQuestionType().equalsIgnoreCase("IMAGE")) {
                final LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.comp_upload, null);
                final ImageView imageView = (ImageView) linearLayout.getChildAt(0);
                imageView.setImageResource(R.drawable.ic_picture);
                imageView.setTag(R.id.column, entity.getQuestionnaireQuestionId());

                Button button = (Button) linearLayout.getChildAt(1);
                button.setText(getString(R.string.chose_photo_file));
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        imagePicker = new ImagePicker.Builder(getActivity())
                                .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                                .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
                                .directory(ImagePicker.Directory.DEFAULT)
                                .extension(ImagePicker.Extension.PNG)
                                .scale(600, 600)
                                .allowMultipleImages(false)
                                .enableDebuggingMode(true)
                                .build();
                        selectedImageView = imageView;

                    }
                });
                container.add(imageView);
                llLayout.addView(linearLayout);

            } else if (entity.getQuestionType().equalsIgnoreCase("VIDEO")) {
                final LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.comp_upload, null);
                final ImageView imageView = (ImageView) linearLayout.getChildAt(0);
                imageView.setImageResource(R.drawable.ic_youtube);
                imageView.setTag(R.id.column, entity.getQuestionnaireQuestionId());
                Button button = (Button) linearLayout.getChildAt(1);
                button.setText(getString(R.string.chose_video_file));
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        videoPicker = new VideoPicker.Builder(getActivity())
                                .mode(VideoPicker.Mode.CAMERA_AND_GALLERY)
                                .directory(VideoPicker.Directory.DEFAULT)
                                .extension(VideoPicker.Extension.MP4)
                                .enableDebuggingMode(true)
                                .build();
                        selectedImageView = imageView;

                    }
                });
                container.add(imageView);
                llLayout.addView(linearLayout);


            } else if (entity.getQuestionType().equalsIgnoreCase("REFERENCE")) {

                LinearLayout llradio = (LinearLayout) inflater.inflate(R.layout.comp_label_spinner, null);
                TextView textView = (TextView) llradio.getChildAt(0);
                textView.setText(entity.getQuestionName());
                llLayout.addView(llradio);
                llradio.setTag(entity.getQuestionnaireQuestionId());

                if (entity.getReferenceValueType().equalsIgnoreCase("PROVINCE")) {
                    //get Data For Spinner
                    ProvinceResponse detailResponse = FakeReferences.getProvinsi();
                    //create Adapater
                    ProvinceAdapter dataAdapter = new ProvinceAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, detailResponse.getEntities());
                    //createSpinner and set Adapater
                    Spinner spinner = (Spinner) inflater.inflate(R.layout.comp_spinner, null);
                    spinner.setTag(entity.getQuestionnaireQuestionId());
                    spinner.setAdapter(dataAdapter);
                    llLayout.addView(spinner);
                    container.add(spinner);

                } else if (entity.getReferenceValueType().equalsIgnoreCase("DISTRICT")) {
                    //get Data For Spinner
                    DistrictResponse detailResponse = FakeReferences.getDistrict();
                    //create Adapater
                    DistrictAdapter dataAdapter = new DistrictAdapter(getActivity(), android.R.layout.simple_spinner_item, detailResponse.getEntities());
                    //createSpinner and set Adapater
                    Spinner spinner = (Spinner) inflater.inflate(R.layout.comp_spinner, null);
                    spinner.setTag(entity.getQuestionnaireQuestionId());
                    spinner.setAdapter(dataAdapter);
                    llLayout.addView(spinner);
                    container.add(spinner);


                } else if (entity.getReferenceValueType().equalsIgnoreCase("SUBDISTRICT")) {
                    //get Data For Spinner
                    SubDistrictResponse detailResponse = FakeReferences.getSubDistrict();
                    //create Adapater
                    SubdistrictAdapter dataAdapter = new SubdistrictAdapter(getActivity(), android.R.layout.simple_spinner_item, detailResponse.getEntities());
                    //createSpinner and set Adapater
                    Spinner spinner = (Spinner) inflater.inflate(R.layout.comp_spinner, null);
                    spinner.setTag(entity.getQuestionnaireQuestionId());
                    spinner.setAdapter(dataAdapter);
                    llLayout.addView(spinner);
                    container.add(spinner);

                } else if (entity.getReferenceValueType().equalsIgnoreCase("VILLAGE")) {
                    //get Data For Spinner
                    VillageResponse detailResponse = FakeReferences.getVillage();
                    //create Adapater
                    VillageAdapter dataAdapter = new VillageAdapter(getActivity(), android.R.layout.simple_spinner_item, detailResponse.getEntities());
                    //createSpinner and set Adapater
                    Spinner spinner = (Spinner) inflater.inflate(R.layout.comp_spinner, null);
                    spinner.setTag(entity.getQuestionnaireQuestionId());
                    spinner.setAdapter(dataAdapter);
                    llLayout.addView(spinner);
                    container.add(spinner);

                }


            }

        }

        Button button = (Button) inflater.inflate(R.layout.comp_button, null);
        button.setText("Submit");
        llLayout.addView(button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (View item : container) {
                    Log.i(TAG, item.getTag().toString());
                    if (item instanceof TextInputEditText) {
                        TextInputEditText text = (TextInputEditText) item;
                        Log.i(TAG, text.getText().toString());
                    } else if (item instanceof Spinner) {
                        Spinner spinner = (Spinner) item;
                        Log.i(TAG, spinner.getSelectedItem().toString());
                    } else if (item instanceof RadioGroup) {
                        RadioGroup radioGroup = (RadioGroup) item;
                        int selectedId = radioGroup.getCheckedRadioButtonId();
                        if (selectedId != -1) {
                            RadioButton radioButton = (RadioButton) view.findViewById(selectedId);
                            Log.i(TAG, radioButton.getText().toString());
                        }

                    } else if (item instanceof RatingBar) {
                        RatingBar ratingBar = (RatingBar) item;
                        Log.i(TAG, ratingBar.getNumStars() + "");
                    } else if (item instanceof ImageView) {
                        ImageView imageView = (ImageView) item;
                        Log.i(TAG, imageView.getTag(PSKSConstanta.IMAGE.CLOUMN) + "");
                    } else if (item instanceof LinearLayout) {
                        LinearLayout linearLayout = (LinearLayout) item;

                        for (int i = 0; i < linearLayout.getChildCount(); i++) {
                            if (linearLayout.getChildAt(i) instanceof CheckBox) {
                                CheckBox checkBox = (CheckBox) linearLayout.getChildAt(i);
                                if (checkBox.isChecked()) {
                                    Log.i(TAG, checkBox.getText().toString());
                                }

                            }
                        }
                    }
                }
            }
        });

        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        List<String> mPaths = null;
        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            mPaths = (List<String>) data.getSerializableExtra(ImagePicker.EXTRA_IMAGE_PATH);
            //Your Code
            selectedImageView.setImageBitmap(BitmapFactory.decodeFile(mPaths.get(0)));
            selectedImageView.setTag(R.id.value, mPaths.get(0));


        } else if (requestCode == VideoPicker.VIDEO_PICKER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            mPaths = (List<String>) data.getSerializableExtra(VideoPicker.EXTRA_VIDEO_PATH);
            Log.i(TAG, mPaths.get(0));
            Bitmap thumb = ThumbnailUtils.createVideoThumbnail(mPaths.get(0), MediaStore.Video.Thumbnails.MINI_KIND);
            selectedImageView.setImageBitmap(thumb);
            selectedImageView.setTag(R.id.value, mPaths.get(0));
        }
    }

    // Date picker
    public void pickDate(EditText et, Context context) {
        if (et != null) {
            selectedEditText = et;
            new DatePickerDialog(context, datePickerListener, calendar
                    .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)).show();
        }
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

            if (selectedEditText != null) {
                selectedEditText.setText(sdf.format(calendar.getTime()));
            }
        }

    };

    // Time picker
    public void pickTime(EditText et, Context context) {
        if (et != null) {
            selectedEditText = et;
            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(context, timePickerListener, hour, minute, true);//Yes 24 hour time
            mTimePicker.show();
        }
    }


    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
            calendar.set(Calendar.HOUR, selectedHour);
            calendar.set(Calendar.MINUTE, selectedMinute);

            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");

            if (selectedEditText != null) {
                selectedEditText.setText(sdf.format(calendar.getTime()));
            }
        }
    };


}
