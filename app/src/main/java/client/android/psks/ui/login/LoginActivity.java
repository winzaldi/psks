package client.android.psks.ui.login;

import android.Manifest;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.github.kayvannj.permission_utils.Func;
import com.github.kayvannj.permission_utils.PermissionUtil;

import client.android.psks.MainActivity;
import client.android.psks.R;
import client.android.psks.model.login.LoginResponse;
import client.android.psks.model.response.AccessToken;
import client.android.psks.service.PSKSServices;
import client.android.psks.service.PSKSTokenService;
import client.android.psks.service.ServiceGenerator;
import client.android.psks.utils.PSKSConstanta;
import client.android.psks.utils.SharedPrefsUtils;
import client.android.psks.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static client.android.psks.BuildConfig.CLIENTID;
import static client.android.psks.BuildConfig.GRANT_TYPE;
import static client.android.psks.BuildConfig.SECRET_KEY;
import static client.android.psks.BuildConfig.TOKEN_PASS;
import static client.android.psks.BuildConfig.TOKEN_USER;

/**
 * Created by winzaldi on 11/29/17.
 */

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();
    public static final String READ_PHONE_STATE = Manifest.permission.READ_PHONE_STATE;
    private PermissionUtil.PermissionRequestObject mPhoneStatePermissionRequest;
    private static final int REQUEST_PHONE_STATE = 1;
    private EditText et_username, et_password;
    private Button btn_login;
    private LoginResponse loginResponse;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        loginResponse = SharedPrefsUtils.getSavedObjectFromPreference(getApplicationContext(), PSKSConstanta.LOGIN_OBJ, LoginResponse.class);

        if (loginResponse != null) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
        }

        setContentView(R.layout.activity_login);
        et_username = findViewById(R.id.et_username);
        et_password = findViewById(R.id.et_password);
        btn_login = findViewById(R.id.btn_login);
        btn_login.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
        et_username.addTextChangedListener(watcher);
        et_password.addTextChangedListener(watcher);

        boolean hasPhoneStatePermission = PermissionUtil.with(this).has(READ_PHONE_STATE);
        if (!hasPhoneStatePermission) {
            mPhoneStatePermissionRequest = PermissionUtil.with(this).request(READ_PHONE_STATE).onAllGranted(new Func() {
                @Override
                protected void call() {
                    Toast.makeText(LoginActivity.this, "Read Phone State Permission Granted", Toast.LENGTH_SHORT).show();
                }
            }).onAnyDenied(new Func() {
                @Override
                protected void call() {
                    Spanned message = new SpannedString("Read Phone State Permission Denied");
                    Utils.showMessageDialog(message, "Permission ", LoginActivity.this);
                }
            }).ask(REQUEST_PHONE_STATE);
        }




        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!Utils.isNetworkConnected(getApplicationContext())) {
                    Toast.makeText(LoginActivity.this, "Internet Not Connected", Toast.LENGTH_SHORT).show();
                    return;
                }

                //call services
                PSKSTokenService services = ServiceGenerator.createService(PSKSTokenService.class, getApplicationContext());

                Call<AccessToken> call = services.getAccessToken(GRANT_TYPE, CLIENTID, SECRET_KEY, TOKEN_USER, TOKEN_PASS);

                call.enqueue(new Callback<AccessToken>() {
                    @Override
                    public void onResponse(Call<AccessToken> call, Response<AccessToken> response) {
                        AccessToken accessToken = response.body();
                        Log.i(TAG, accessToken.toString());
                        SharedPrefsUtils.saveToPrefs(getApplicationContext(), PSKSConstanta.NEKOT_APP, accessToken.getAccessToken());
                        login(accessToken.getAccessToken(), et_username.getText().toString(), et_password.getText().toString());

                    }

                    @Override
                    public void onFailure(Call<AccessToken> call, Throwable t) {
                        Log.i(TAG, "Access Token Failure" + t.getLocalizedMessage());
                    }
                });

            }
        });


    }


    private void login(String token, String username, String password) {

        PSKSServices psksServices = ServiceGenerator.createService(PSKSServices.class, getApplicationContext(), token);


        Call<LoginResponse> callpsks = psksServices.login(username, password, Utils.getDeviceIMEI(getApplicationContext()));

        callpsks.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.code() == 200) {
                    Log.i(TAG, response.body().toString());
                    SharedPrefsUtils.saveObjectToSharedPreference(getApplicationContext(), PSKSConstanta.LOGIN_OBJ, response.body());
                    Toast.makeText(LoginActivity.this, "Login Success", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                } else {
                    //Toast.makeText(LoginActivity.this, "Login Failed", Toast.LENGTH_SHORT).show();
                    Spanned message = new SpannedString("An error has occurred during your attempt to sign in, please try again.");
                    Utils.showMessageDialog(message, "Login Failed", LoginActivity.this);
                    Log.i(TAG, response.raw().message());
                }

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.i(TAG, t.getLocalizedMessage());
            }
        });

    }

    TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if ((TextUtils.isEmpty(et_username.getText()))
                    || (TextUtils.isEmpty(et_password.getText()))) {
                btn_login.setEnabled(false);
                btn_login.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
            } else {
                btn_login.setEnabled(true);
                btn_login.getBackground().setColorFilter(null);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        //SharedPrefsUtils.removeFromPrefs(this, PSKSConstanta.LOGIN_OBJ);
        loginResponse = SharedPrefsUtils.getSavedObjectFromPreference(getApplicationContext(), PSKSConstanta.LOGIN_OBJ, LoginResponse.class);

        if (loginResponse != null) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }
}
