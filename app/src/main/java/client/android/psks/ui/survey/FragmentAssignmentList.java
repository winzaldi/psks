package client.android.psks.ui.survey;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.gson.Gson;

import client.android.psks.MainActivity;
import client.android.psks.R;
import client.android.psks.model.login.LoginResponse;
import client.android.psks.model.survey.SurveyAssignmentResponse;
import client.android.psks.service.PSKSServices;
import client.android.psks.service.ServiceGenerator;
import client.android.psks.utils.PSKSConstanta;
import client.android.psks.utils.SharedPrefsUtils;
import client.android.psks.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by winzaldi on 11/29/17.
 */

public class FragmentAssignmentList extends Fragment  implements MainActivity.OnBackPressedListener {
    private RecyclerView recyclerView;
    private AssigmentListAdapater adapater;
    private LoginResponse user;
    private CoordinatorLayout coordinatorLayout;
    private ProgressBar progressBar;
    private  SurveyAssignmentResponse surveyResponse;


    public static FragmentAssignmentList newInstance() {
        return new FragmentAssignmentList();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(PSKSConstanta.LIST_SURVEY,surveyResponse);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if(savedInstanceState!=null){
            adapater.setListOfSurvey((SurveyAssignmentResponse) savedInstanceState.get(PSKSConstanta.LIST_SURVEY));
            recyclerView.setAdapter(adapater);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        final View view = inflater.inflate(R.layout.fragment_assignment_list_survey, container, false);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.assignment);
        ((MainActivity) getActivity()).setOnBackPressedListener(this);
        progressBar = view.findViewById(R.id.progressBar);
        user = new Gson().fromJson(SharedPrefsUtils.getFromPrefs(getActivity(), PSKSConstanta.LOGIN_OBJ, ""), LoginResponse.class);

        coordinatorLayout = view.findViewById(R.id.coordinator);
        recyclerView = view.findViewById(R.id.recycleview_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapater = new AssigmentListAdapater(getActivity());
        recyclerView.setHasFixedSize(true);

        if(savedInstanceState!=null){
            adapater.setListOfSurvey((SurveyAssignmentResponse) savedInstanceState.get(PSKSConstanta.LIST_SURVEY));
            recyclerView.setAdapter(adapater);
        }else{
            //SurveyAssignmentResponse response = FakeUtils.getSurveyList();
            PSKSServices services = ServiceGenerator.createService(PSKSServices.class, getContext());

            Call<SurveyAssignmentResponse> callsurvey = services.getListSurveyAssignment(user.getEntity().getUserId());
            progressBar.setVisibility(View.VISIBLE);
            callsurvey.enqueue(new Callback<SurveyAssignmentResponse>() {
                @Override
                public void onResponse(Call<SurveyAssignmentResponse> call, Response<SurveyAssignmentResponse> response) {
                    if (response.code() == 200) {
                        surveyResponse = response.body();
                        adapater.setListOfSurvey(surveyResponse);
                        recyclerView.setAdapter(adapater);
                    }else if (response.code() == 500) {
                        Utils.showSnakbarMessage(coordinatorLayout,"Gagal mendapatkan daftar survey");
                    }
                    progressBar.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onFailure(Call<SurveyAssignmentResponse> call, Throwable t) {
                    progressBar.setVisibility(View.INVISIBLE);
                    Utils.showSnakbarMessage(coordinatorLayout, "Gagal mendapatkan daftar survey");
                }
            });

        }




        return view;

    }

    @Override
    public void doBack() {
        //Toast.makeText(getActivity(), "Fragment Member", Toast.LENGTH_SHORT).show();
        Utils.showSnakbarMessageOK(coordinatorLayout,"Anda sudah dihalaman utama");
    }
}
