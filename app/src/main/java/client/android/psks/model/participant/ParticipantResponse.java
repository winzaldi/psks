package client.android.psks.model.participant;

import java.util.Arrays;

import client.android.psks.model.response.BaseResponse;

/**
 * Created by winzaldi on 11/20/17.
 */

public class ParticipantResponse extends BaseResponse  {
  private  Entities[] entities;

    public Entities[] getEntities() {
        return entities;
    }

    public void setEntities(Entities[] entities) {
        this.entities = entities;
    }

    @Override
    public String toString() {
        return "ParticipantResponse{" +
                "entities=" + Arrays.toString(entities) +
                '}';
    }
}
