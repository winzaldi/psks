package client.android.psks.model.lembaga;

import client.android.psks.model.response.BaseResponse;

/**
 * Created by winzaldi on 1/21/18.
 */

public class KelembagaanResponse  extends BaseResponse{

    private Entity entity;


    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    @Override
    public String toString() {
        return "KelembagaanResponse{" +
                "entity=" + entity +
                '}';
    }
}
