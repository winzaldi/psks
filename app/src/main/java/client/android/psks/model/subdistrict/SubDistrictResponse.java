package client.android.psks.model.subdistrict;

import java.util.Arrays;

import client.android.psks.model.response.BaseResponse;

/**
 * Created by winzaldi on 11/20/17.
 */

public class SubDistrictResponse extends BaseResponse{
    private Entities[] entities;

    public Entities[] getEntities() {
        return entities;
    }

    public void setEntities(Entities[] entities) {
        this.entities = entities;
    }

    @Override
    public String toString() {
        return "SubDistrictResponse{" +
                "entities=" + Arrays.toString(entities) +
                '}';
    }
}
