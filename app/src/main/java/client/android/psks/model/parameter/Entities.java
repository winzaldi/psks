package client.android.psks.model.parameter;

/**
 * Created by winzaldi on 11/29/17.
 */

public class Entities {
    private String seqNo;

    private String description;

    private MParameterGroupValuePK mParameterGroupValuePK;

    public String getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(String seqNo) {
        this.seqNo = seqNo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MParameterGroupValuePK getmParameterGroupValuePK() {
        return mParameterGroupValuePK;
    }

    public void setmParameterGroupValuePK(MParameterGroupValuePK mParameterGroupValuePK) {
        this.mParameterGroupValuePK = mParameterGroupValuePK;
    }

    @Override
    public String toString() {
        return description;
    }
}
