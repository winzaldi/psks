package client.android.psks.model.parameter;

import java.util.Arrays;

import client.android.psks.model.response.BaseResponse;

/**
 * Created by winzaldi on 11/29/17.
 */

public class ParameterGroupResponse extends BaseResponse  {

    private Entities[] entities;

    public Entities[] getEntities() {
        return entities;
    }

    public void setEntities(Entities[] entities) {
        this.entities = entities;
    }

    @Override
    public String toString() {
        return "ParameterGroupResponse{" +
                "entities=" + Arrays.toString(entities) +
                '}';
    }
}
