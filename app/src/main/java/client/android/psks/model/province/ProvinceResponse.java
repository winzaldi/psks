package client.android.psks.model.province;

import java.util.Arrays;

import client.android.psks.model.response.BaseResponse;

/**
 * Created by winzaldi on 11/20/17.
 */

public class ProvinceResponse extends BaseResponse {

        private Entities[] entities;

    public Entities[] getEntities() {
        return entities;
    }

    public void setEntities(Entities[] entities) {
        this.entities = entities;
    }


    @Override
    public String toString() {
        return "ProvinceResponse{" +
                "entities=" + Arrays.toString(entities) +
                '}';
    }
}
