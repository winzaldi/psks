package client.android.psks.model.question;


import java.util.Arrays;

import client.android.psks.model.response.BaseResponse;

/**
 * Created by winzaldi on 11/15/17.
 */

public class QuestionDetailResponse extends BaseResponse{


    private Entities[] entities;



    public Entities[] getEntities() {
        return entities;
    }

    public void setEntities(Entities[] entities) {
        this.entities = entities;
    }

    @Override
    public String toString() {
        return "QuestionDetailResponse{" +
                "entities=" + Arrays.toString(entities) +
                '}';
    }
}
