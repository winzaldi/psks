package client.android.psks.model.survey;

import java.io.Serializable;

/**
 * Created by winzaldi on 11/15/17.
 */

public class Questionnaire implements Serializable{

    private String questionnaireDesc;

    private String questionnaireId;

    private String questionnaireName;

    public String getQuestionnaireDesc() {
        return questionnaireDesc;
    }

    public void setQuestionnaireDesc(String questionnaireDesc) {
        this.questionnaireDesc = questionnaireDesc;
    }

    public String getQuestionnaireId() {
        return questionnaireId;
    }

    public void setQuestionnaireId(String questionnaireId) {
        this.questionnaireId = questionnaireId;
    }

    public String getQuestionnaireName() {
        return questionnaireName;
    }

    public void setQuestionnaireName(String questionnaireName) {
        this.questionnaireName = questionnaireName;
    }

    @Override
    public String toString() {
        return "Questionnaire{" +
                "questionnaireDesc='" + questionnaireDesc + '\'' +
                ", questionnaireId='" + questionnaireId + '\'' +
                ", questionnaireName='" + questionnaireName + '\'' +
                '}';
    }
}
