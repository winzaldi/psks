package client.android.psks.model.parameter;

/**
 * Created by winzaldi on 11/29/17.
 */

public class MParameterGroupValuePK {

    private String groupCode;

    private String valueCode;

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getValueCode() {
        return valueCode;
    }

    public void setValueCode(String valueCode) {
        this.valueCode = valueCode;
    }

    @Override
    public String toString() {
        return "MParameterGroupValuePK{" +
                "groupCode='" + groupCode + '\'' +
                ", valueCode='" + valueCode + '\'' +
                '}';
    }
}
