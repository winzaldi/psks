package client.android.psks.model.quitioner;

/**
 * Created by winzaldi on 11/15/17.
 */

public class Entities {

    private String usedDescription;

    private String shuffleOptionOrder;

    private String usedDataValidation;

    private String questionnaireQuestionId;

    private String questionName;

    private String questionType;

    private String required;

    private String linearToNum;
    private String linearStartNum;
    private String linearLabelStart;
    private String linearLabelTo;
    private String referenceValueType;




    public String getUsedDescription() {
        return usedDescription;
    }

    public void setUsedDescription(String usedDescription) {
        this.usedDescription = usedDescription;
    }

    public String getShuffleOptionOrder() {
        return shuffleOptionOrder;
    }

    public void setShuffleOptionOrder(String shuffleOptionOrder) {
        this.shuffleOptionOrder = shuffleOptionOrder;
    }

    public String getUsedDataValidation() {
        return usedDataValidation;
    }

    public void setUsedDataValidation(String usedDataValidation) {
        this.usedDataValidation = usedDataValidation;
    }

    public String getQuestionnaireQuestionId() {
        return questionnaireQuestionId;
    }

    public void setQuestionnaireQuestionId(String questionnaireQuestionId) {
        this.questionnaireQuestionId = questionnaireQuestionId;
    }

    public String getQuestionName() {
        return questionName;
    }

    public void setQuestionName(String questionName) {
        this.questionName = questionName;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public String getRequired() {
        return required;
    }

    public void setRequired(String required) {
        this.required = required;
    }

    public String getLinearToNum() {
        return linearToNum;
    }

    public void setLinearToNum(String linearToNum) {
        this.linearToNum = linearToNum;
    }

    public String getLinearStartNum() {
        return linearStartNum;
    }

    public void setLinearStartNum(String linearStartNum) {
        this.linearStartNum = linearStartNum;
    }

    public String getLinearLabelStart() {
        return linearLabelStart;
    }

    public void setLinearLabelStart(String linearLabelStart) {
        this.linearLabelStart = linearLabelStart;
    }

    public String getLinearLabelTo() {
        return linearLabelTo;
    }

    public void setLinearLabelTo(String linearLabelTo) {
        this.linearLabelTo = linearLabelTo;
    }

    public String getReferenceValueType() {
        return referenceValueType;
    }

    public void setReferenceValueType(String referenceValueType) {
        this.referenceValueType = referenceValueType;
    }

    @Override
    public String toString() {
        return "Entities{" +
                "usedDescription='" + usedDescription + '\'' +
                ", shuffleOptionOrder='" + shuffleOptionOrder + '\'' +
                ", usedDataValidation='" + usedDataValidation + '\'' +
                ", questionnaireQuestionId='" + questionnaireQuestionId + '\'' +
                ", questionName='" + questionName + '\'' +
                ", questionType='" + questionType + '\'' +
                ", required='" + required + '\'' +
                ", linearToNum='" + linearToNum + '\'' +
                ", linearStartNum='" + linearStartNum + '\'' +
                ", linearLabelStart='" + linearLabelStart + '\'' +
                ", linearLabelTo='" + linearLabelTo + '\'' +
                ", referenceValueType='" + referenceValueType + '\'' +
                '}';
    }
}
