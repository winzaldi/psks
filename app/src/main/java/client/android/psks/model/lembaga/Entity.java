package client.android.psks.model.lembaga;

/**
 * Created by winzaldi on 1/21/18.
 */

public class Entity {

    private String idLembaga;
    private String namaLembaga;
    private String alamatLembaga;

    private String provinceCode;
    private String districtCode;
    private String subDistrictCode;
    private String villageCode;

    private String psksMemberId;
    private String ketua;
    private String wakilKetua;
    private String sekretaris;
    private String wakilSekretaris;
    private String bendahara;
    private String wakilBendahara;
    private String sieOlahraga;
    private String sieHumas;
    private String sieSeniBudaya;
    private String siePerlengkapanUmum;
    private String sieKerohanian;
    private String sieKomunikasiPublikasi;


    public String getIdLembaga() {
        return idLembaga;
    }

    public void setIdLembaga(String idLembaga) {
        this.idLembaga = idLembaga;
    }

    public String getNamaLembaga() {
        return namaLembaga;
    }

    public void setNamaLembaga(String namaLembaga) {
        this.namaLembaga = namaLembaga;
    }

    public String getAlamatLembaga() {
        return alamatLembaga;
    }

    public void setAlamatLembaga(String alamatLembaga) {
        this.alamatLembaga = alamatLembaga;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    public String getSubDistrictCode() {
        return subDistrictCode;
    }

    public void setSubDistrictCode(String subDistrictCode) {
        this.subDistrictCode = subDistrictCode;
    }

    public String getVillageCode() {
        return villageCode;
    }

    public void setVillageCode(String villageCode) {
        this.villageCode = villageCode;
    }

    public String getPsksMemberId() {
        return psksMemberId;
    }

    public void setPsksMemberId(String psksMemberId) {
        this.psksMemberId = psksMemberId;
    }

    public String getKetua() {
        return ketua;
    }

    public void setKetua(String ketua) {
        this.ketua = ketua;
    }

    public String getWakilKetua() {
        return wakilKetua;
    }

    public void setWakilKetua(String wakilKetua) {
        this.wakilKetua = wakilKetua;
    }

    public String getSekretaris() {
        return sekretaris;
    }

    public void setSekretaris(String sekretaris) {
        this.sekretaris = sekretaris;
    }

    public String getWakilSekretaris() {
        return wakilSekretaris;
    }

    public void setWakilSekretaris(String wakilSekretaris) {
        this.wakilSekretaris = wakilSekretaris;
    }

    public String getBendahara() {
        return bendahara;
    }

    public void setBendahara(String bendahara) {
        this.bendahara = bendahara;
    }

    public String getWakilBendahara() {
        return wakilBendahara;
    }

    public void setWakilBendahara(String wakilBendahara) {
        this.wakilBendahara = wakilBendahara;
    }

    public String getSieOlahraga() {
        return sieOlahraga;
    }

    public void setSieOlahraga(String sieOlahraga) {
        this.sieOlahraga = sieOlahraga;
    }

    public String getSieHumas() {
        return sieHumas;
    }

    public void setSieHumas(String sieHumas) {
        this.sieHumas = sieHumas;
    }

    public String getSieSeniBudaya() {
        return sieSeniBudaya;
    }

    public void setSieSeniBudaya(String sieSeniBudaya) {
        this.sieSeniBudaya = sieSeniBudaya;
    }

    public String getSiePerlengkapanUmum() {
        return siePerlengkapanUmum;
    }

    public void setSiePerlengkapanUmum(String siePerlengkapanUmum) {
        this.siePerlengkapanUmum = siePerlengkapanUmum;
    }

    public String getSieKerohanian() {
        return sieKerohanian;
    }

    public void setSieKerohanian(String sieKerohanian) {
        this.sieKerohanian = sieKerohanian;
    }

    public String getSieKomunikasiPublikasi() {
        return sieKomunikasiPublikasi;
    }

    public void setSieKomunikasiPublikasi(String sieKomunikasiPublikasi) {
        this.sieKomunikasiPublikasi = sieKomunikasiPublikasi;
    }

    @Override
    public String toString() {
        return "Entity{" +
                "idLembaga='" + idLembaga + '\'' +
                ", namaLembaga='" + namaLembaga + '\'' +
                ", alamatLembaga='" + alamatLembaga + '\'' +
                ", provinceCode='" + provinceCode + '\'' +
                ", districtCode='" + districtCode + '\'' +
                ", subDistrictCode='" + subDistrictCode + '\'' +
                ", villageCode='" + villageCode + '\'' +
                ", psksMemberId='" + psksMemberId + '\'' +
                ", ketua='" + ketua + '\'' +
                ", wakilKetua='" + wakilKetua + '\'' +
                ", sekretaris='" + sekretaris + '\'' +
                ", wakilSekretaris='" + wakilSekretaris + '\'' +
                ", bendahara='" + bendahara + '\'' +
                ", wakilBendahara='" + wakilBendahara + '\'' +
                ", sieOlahraga='" + sieOlahraga + '\'' +
                ", sieHumas='" + sieHumas + '\'' +
                ", sieSeniBudaya='" + sieSeniBudaya + '\'' +
                ", siePerlengkapanUmum='" + siePerlengkapanUmum + '\'' +
                ", sieKerohanian='" + sieKerohanian + '\'' +
                ", sieKomunikasiPublikasi='" + sieKomunikasiPublikasi + '\'' +
                '}';
    }
}
