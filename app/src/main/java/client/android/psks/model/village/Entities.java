package client.android.psks.model.village;

/**
 * Created by winzaldi on 11/20/17.
 */

public class Entities {

    private String isVillage;

    private String subDistrictCode;

    private String villageCode;

    private String zipCode;

    private String villageName;

    public String getIsVillage() {
        return isVillage;
    }

    public void setIsVillage(String isVillage) {
        this.isVillage = isVillage;
    }

    public String getSubDistrictCode() {
        return subDistrictCode;
    }

    public void setSubDistrictCode(String subDistrictCode) {
        this.subDistrictCode = subDistrictCode;
    }

    public String getVillageCode() {
        return villageCode;
    }

    public void setVillageCode(String villageCode) {
        this.villageCode = villageCode;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getVillageName() {
        return villageName;
    }

    public void setVillageName(String villageName) {
        this.villageName = villageName;
    }

    @Override
    public String toString() {
        return villageName;
    }
}
