package client.android.psks.model.survey;

import java.io.Serializable;

/**
 * Created by winzaldi on 11/15/17.
 */

public class SurveyAssignment implements Serializable{

    private String surveyorCode;

    private String targetDate;

    private String surveyAssignmentId;

    private String assignmentDate;

    private String respondenTarget;

    private String notes;

    private String surveyScheduleId;

    public String getSurveyorCode() {
        return surveyorCode;
    }

    public void setSurveyorCode(String surveyorCode) {
        this.surveyorCode = surveyorCode;
    }

    public String getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(String targetDate) {
        this.targetDate = targetDate;
    }

    public String getSurveyAssignmentId() {
        return surveyAssignmentId;
    }

    public void setSurveyAssignmentId(String surveyAssignmentId) {
        this.surveyAssignmentId = surveyAssignmentId;
    }

    public String getAssignmentDate() {
        return assignmentDate;
    }

    public void setAssignmentDate(String assignmentDate) {
        this.assignmentDate = assignmentDate;
    }

    public String getRespondenTarget() {
        return respondenTarget;
    }

    public void setRespondenTarget(String respondenTarget) {
        this.respondenTarget = respondenTarget;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getSurveyScheduleId() {
        return surveyScheduleId;
    }

    public void setSurveyScheduleId(String surveyScheduleId) {
        this.surveyScheduleId = surveyScheduleId;
    }
}
