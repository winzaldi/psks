package client.android.psks.model.quitioner;

import java.util.Arrays;

import client.android.psks.model.response.BaseResponse;

/**
 * Created by winzaldi on 11/15/17.
 */

public class QuestionnaireResponse extends BaseResponse{


    private Entities[] entities;



    public Entities[] getEntities() {
        return entities;
    }

    public void setEntities(Entities[] entities) {
        this.entities = entities;
    }

    @Override
    public String toString() {
        return "QuestionnaireResponse{" +
                "entities=" + Arrays.toString(getEntities()) +
                '}';
    }
}
