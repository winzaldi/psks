package client.android.psks.model.member;

/**
 * Created by winzaldi on 11/15/17.
 */

public class Entity {

    private String status;

    private String villageCode;

    private String provinceCode;

    private String occupationCode;

    private String psksMemberId;

    private String subDistrictCode;

    private String districtCode;

    private String phoneNumber;

    private String lastEducationCode;

    private String photoId;

    private String address;

    private String gender;

    private String isCoordinator;

    private String participantType;

    private String psksName;

    private String psksNo;

    private String psksType;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVillageCode() {
        return villageCode;
    }

    public void setVillageCode(String villageCode) {
        this.villageCode = villageCode;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getOccupationCode() {
        return occupationCode;
    }

    public void setOccupationCode(String occupationCode) {
        this.occupationCode = occupationCode;
    }

    public String getPsksMemberId() {
        return psksMemberId;
    }

    public void setPsksMemberId(String psksMemberId) {
        this.psksMemberId = psksMemberId;
    }

    public String getSubDistrictCode() {
        return subDistrictCode;
    }

    public void setSubDistrictCode(String subDistrictCode) {
        this.subDistrictCode = subDistrictCode;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLastEducationCode() {
        return lastEducationCode;
    }

    public void setLastEducationCode(String lastEducationCode) {
        this.lastEducationCode = lastEducationCode;
    }

    public String getPhotoId() {
        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIsCoordinator() {
        return isCoordinator;
    }

    public void setIsCoordinator(String isCoordinator) {
        this.isCoordinator = isCoordinator;
    }

    public String getParticipantType() {
        return participantType;
    }

    public void setParticipantType(String participantType) {
        this.participantType = participantType;
    }

    public String getPsksName() {
        return psksName;
    }

    public void setPsksName(String psksName) {
        this.psksName = psksName;
    }

    public String getPsksNo() {
        return psksNo;
    }

    public void setPsksNo(String psksNo) {
        this.psksNo = psksNo;
    }

    public String getPsksType() {
        return psksType;
    }

    public void setPsksType(String psksType) {
        this.psksType = psksType;
    }

    @Override
    public String toString() {
        return "Entity{" +
                "status='" + status + '\'' +
                ", villageCode='" + villageCode + '\'' +
                ", provinceCode='" + provinceCode + '\'' +
                ", occupationCode='" + occupationCode + '\'' +
                ", psksMemberId='" + psksMemberId + '\'' +
                ", subDistrictCode='" + subDistrictCode + '\'' +
                ", districtCode='" + districtCode + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", lastEducationCode='" + lastEducationCode + '\'' +
                ", photoId='" + photoId + '\'' +
                ", address='" + address + '\'' +
                ", gender='" + gender + '\'' +
                ", isCoordinator='" + isCoordinator + '\'' +
                ", participantType='" + participantType + '\'' +
                ", psksName='" + psksName + '\'' +
                ", psksNo='" + psksNo + '\'' +
                ", psksType='" + psksType + '\'' +
                '}';
    }
}
