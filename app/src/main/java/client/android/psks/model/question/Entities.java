package client.android.psks.model.question;

/**
 * Created by winzaldi on 11/15/17.
 */

public class Entities {

    private String mQuestionnaireQuestionDtl;

    private String labelText;

    private String columnText;

    public String getMQuestionnaireQuestionDtl ()
    {
        return mQuestionnaireQuestionDtl;
    }

    public void setMQuestionnaireQuestionDtl (String mQuestionnaireQuestionDtl)
    {
        this.mQuestionnaireQuestionDtl = mQuestionnaireQuestionDtl;
    }

    public String getLabelText ()
    {
        return labelText;
    }

    public void setLabelText (String labelText)
    {
        this.labelText = labelText;
    }

    public String getColumnText ()
    {
        return columnText;
    }

    public void setColumnText (String columnText)
    {
        this.columnText = columnText;
    }

    @Override
    public String toString()
    {
        return labelText;
    }
}
