package client.android.psks.model.response;

/**
 * Created by winzaldi on 11/16/17.
 */
public class BaseResponse {

    private String responseMessage;

    private String responseCode;

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    @Override
    public String toString() {
        return "BaseResponse{" +
                "responseMessage='" + responseMessage + '\'' +
                ", responseCode='" + responseCode + '\'' +
                '}';
    }
}
