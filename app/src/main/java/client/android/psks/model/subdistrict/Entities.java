package client.android.psks.model.subdistrict;

/**
 * Created by winzaldi on 11/20/17.
 */

public class Entities {

    private String subDistrictCode;

    private String districtCode;

    private String subDistrictName;


    public String getSubDistrictCode() {
        return subDistrictCode;
    }

    public void setSubDistrictCode(String subDistrictCode) {
        this.subDistrictCode = subDistrictCode;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    public String getSubDistrictName() {
        return subDistrictName;
    }

    public void setSubDistrictName(String subDistrictName) {
        this.subDistrictName = subDistrictName;
    }

    @Override
    public String toString() {
        return subDistrictName;
    }
}
