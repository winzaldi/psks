package client.android.psks.model.village;

import java.util.Arrays;

import client.android.psks.model.response.BaseResponse;

/**
 * Created by winzaldi on 11/20/17.
 */

public class VillageResponse extends BaseResponse {

    private Entities[] entities;

    public Entities[] getEntities() {
        return entities;
    }

    public void setEntities(Entities[] entities) {
        this.entities = entities;
    }

    @Override
    public String toString() {
        return "VillageResponse{" +
                "entities=" + Arrays.toString(entities) +
                '}';
    }
}
