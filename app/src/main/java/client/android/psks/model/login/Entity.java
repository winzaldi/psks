package client.android.psks.model.login;

/**
 * Created by winzaldi on 11/15/17.
 */

public class Entity {

    private String villageCode;

    private String provinceCode;

    private String firstLogin;

    private String accountExpired;

    private String surveyorName;

    private String surveyorCode;

    private String subDistrictCode;

    private String phoneNumber;

    private String photoId;

    private String lastPasswordUpdate;

    private String userId;

    private String gender;

    private String userName;

    private String isCoordinator;

    private String login;

    private String psksName;

    private String lastAccessFrom;

    private String status;

    private String accountLocked;

    private String psksMemberId;

    private String districtCode;

    private String email;

    private String accountEnabled;

    private String address;

    private String participantType;

    private String psksNo;

    private String lastAccessDate;

    private String psksType;

    private String idLembaga;

    public String getVillageCode() {
        return villageCode;
    }

    public void setVillageCode(String villageCode) {
        this.villageCode = villageCode;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(String firstLogin) {
        this.firstLogin = firstLogin;
    }

    public String getAccountExpired() {
        return accountExpired;
    }

    public void setAccountExpired(String accountExpired) {
        this.accountExpired = accountExpired;
    }

    public String getSurveyorName() {
        return surveyorName;
    }

    public void setSurveyorName(String surveyorName) {
        this.surveyorName = surveyorName;
    }

    public String getSurveyorCode() {
        return surveyorCode;
    }

    public void setSurveyorCode(String surveyorCode) {
        this.surveyorCode = surveyorCode;
    }

    public String getSubDistrictCode() {
        return subDistrictCode;
    }

    public void setSubDistrictCode(String subDistrictCode) {
        this.subDistrictCode = subDistrictCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhotoId() {
        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public String getLastPasswordUpdate() {
        return lastPasswordUpdate;
    }

    public void setLastPasswordUpdate(String lastPasswordUpdate) {
        this.lastPasswordUpdate = lastPasswordUpdate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getIsCoordinator() {
        return isCoordinator;
    }

    public void setIsCoordinator(String isCoordinator) {
        this.isCoordinator = isCoordinator;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPsksName() {
        return psksName;
    }

    public void setPsksName(String psksName) {
        this.psksName = psksName;
    }

    public String getLastAccessFrom() {
        return lastAccessFrom;
    }

    public void setLastAccessFrom(String lastAccessFrom) {
        this.lastAccessFrom = lastAccessFrom;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAccountLocked() {
        return accountLocked;
    }

    public void setAccountLocked(String accountLocked) {
        this.accountLocked = accountLocked;
    }

    public String getPsksMemberId() {
        return psksMemberId;
    }

    public void setPsksMemberId(String psksMemberId) {
        this.psksMemberId = psksMemberId;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAccountEnabled() {
        return accountEnabled;
    }

    public void setAccountEnabled(String accountEnabled) {
        this.accountEnabled = accountEnabled;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getParticipantType() {
        return participantType;
    }

    public void setParticipantType(String participantType) {
        this.participantType = participantType;
    }

    public String getPsksNo() {
        return psksNo;
    }

    public void setPsksNo(String psksNo) {
        this.psksNo = psksNo;
    }

    public String getLastAccessDate() {
        return lastAccessDate;
    }

    public void setLastAccessDate(String lastAccessDate) {
        this.lastAccessDate = lastAccessDate;
    }

    public String getPsksType() {
        return psksType;
    }

    public void setPsksType(String psksType) {
        this.psksType = psksType;
    }


    public String getIdLembaga() {
        return idLembaga;
    }

    public void setIdLembaga(String idLembaga) {
        this.idLembaga = idLembaga;
    }

    @Override
    public String toString() {
        return "Entity [villageCode = " + villageCode + ", provinceCode = " + provinceCode + ", firstLogin = " + firstLogin + ", accountExpired = " + accountExpired + ", surveyorName = " + surveyorName + ", surveyorCode = " + surveyorCode + ", subDistrictCode = " + subDistrictCode + ", phoneNumber = " + phoneNumber + ", photoId = " + photoId + ", lastPasswordUpdate = " + lastPasswordUpdate + ", userId = " + userId + ", gender = " + gender + ", userName = " + userName + ", isCoordinator = " + isCoordinator + ", login = " + login + ", psksName = " + psksName + ", lastAccessFrom = " + lastAccessFrom + ", status = " + status + ", accountLocked = " + accountLocked + ", psksMemberId = " + psksMemberId + ", districtCode = " + districtCode + ", email = " + email + ", accountEnabled = " + accountEnabled + ", address = " + address + ", participantType = " + participantType + ", psksNo = " + psksNo + ", lastAccessDate = " + lastAccessDate + ", psksType = " + psksType + "]";
    }
}
