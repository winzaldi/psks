package client.android.psks;

import android.widget.LinearLayout;

import client.android.psks.model.quitioner.Entities;

/**
 * Created by winzaldi on 11/25/17.
 */

public class FormStepFragment {// extends BaseFragment implements BlockingStep {
    //public class FormStepFragment  extends BaseFragment implements BlockingStep {
    private static final String TAG = FormStepFragment.class.getSimpleName();
    private LinearLayout llLayout;
    private static Entities[] entities;
    public static  FormStepFragment newInstance (Entities[] et){
        entities = et;
        return new FormStepFragment();
    }

//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
//        super.onCreateView(inflater, container, savedInstanceState);
//        View view = inflater.inflate(R.layout.fragment_stepper,container,false);
//
//        llLayout = view.findViewById(R.id.llLayout);
//
//        for (int i = 0; i < entities.length; i++) {
//            Entities entity = entities[i];
//
//
//            if (entity.getQuestionType().equalsIgnoreCase("SHORT_TEXT")) {
//                TextInputLayout textInputLayout = (TextInputLayout) inflater.inflate(R.layout.comp_short_text, null);
//                textInputLayout.setHint(entity.getQuestionName());
//                final TextInputEditText textInputEditText = (TextInputEditText) textInputLayout.getEditText();
//                textInputEditText.setId(Utils.generateViewId());
//                //set variable name
////                textInputEditText.setTag(entity.getQuestionnaireQuestionId());
////                if (entity.getRequired().equalsIgnoreCase("true")) {
////                    textInputEditText.addTextChangedListener(new TextEmptyValidation(textInputEditText));
////                }
//
//
//                //add child view to parent
//                llLayout.addView(textInputLayout);
//                //container.add(textInputEditText);
//            } else if (entity.getQuestionType().equalsIgnoreCase("PARAGRAPH")) {
//
//                TextInputLayout textInputLayout = (TextInputLayout) inflater.inflate(R.layout.comp_paragraph, null);
//                textInputLayout.setHint(entity.getQuestionName());
//                final TextInputEditText textInputEditText = (TextInputEditText) textInputLayout.getEditText();
//                textInputEditText.setId(Utils.generateViewId());
//                //set variable name
//                textInputEditText.setTag(entity.getQuestionnaireQuestionId());
//                if (entity.getRequired().equalsIgnoreCase("true")) {
//                    textInputEditText.addTextChangedListener(new TextEmptyValidation(textInputEditText));
//                }
//
//                llLayout.addView(textInputLayout);
//                //container.add(textInputEditText);
//            } else if (entity.getQuestionType().equalsIgnoreCase("DROPDOWN")) {
//                LinearLayout llradio = (LinearLayout) inflater.inflate(R.layout.comp_label_spinner, null);
//                TextView textView = (TextView) llradio.getChildAt(0);
//                textView.setText(entity.getQuestionName());
//                llLayout.addView(llradio);
//                llradio.setTag(entity.getQuestionnaireQuestionId());
//                //get Data For Spinner
//                QuestionDetailResponse detailResponse = FakeUtils.getQuestionDetail(FakeUtils.QDETAIL);
//                //create Adapater
//                QuestionDetailAdapter dataAdapter = new QuestionDetailAdapter(getContext(), android.R.layout.simple_spinner_item, detailResponse.getEntities());
//                //createSpinner and set Adapater
//                Spinner spinner = (Spinner) inflater.inflate(R.layout.comp_dropdown, null);
//                spinner.setTag(entity.getQuestionnaireQuestionId());
//                spinner.setAdapter(dataAdapter);
//
//                llLayout.addView(spinner);
//                //container.add(spinner);
//            } else if (entity.getQuestionType().equalsIgnoreCase("CHECKBOX")) {
//                LinearLayout llcheckbox = (LinearLayout) inflater.inflate(R.layout.comp_label_checkbox, null);
//                TextView textView = (TextView) llcheckbox.getChildAt(0);
//                textView.setText(entity.getQuestionName());
//                llLayout.addView(llcheckbox);
//                llcheckbox.setTag(entity.getQuestionnaireQuestionId());
//                QuestionDetailResponse detailResponse = FakeUtils.getQuestionDetail(FakeUtils.CHECKBOX_HOBI_JSON);
//                for (int idx = 0; idx < detailResponse.getEntities().length; idx++) {
//                    client.android.psks.model.question.Entities item = detailResponse.getEntities()[idx];
//                    CheckBox checkBox = (CheckBox) inflater.inflate(R.layout.comp_checkbox, null);
//                    checkBox.setTag(item.getMQuestionnaireQuestionDtl());
//                    checkBox.setText(item.getLabelText());
//                    llcheckbox.addView(checkBox);
//                    //container.add(checkBox);
//                }
//
//            } else if (entity.getQuestionType().equalsIgnoreCase("CHOICE")) {
////                RadioGroup radioGroup =(RadioGroup) inflater.inflate(R.layout.comp_choice,null);
////                radioGroup.setTag(entity.getQuestionnaireQuestionId());
////                llLayout.addView(radioGroup);
////                container.add(radioGroup);
//
//                LinearLayout lradio = (LinearLayout) inflater.inflate(R.layout.comp_label_radio, null);
//                TextView textView = (TextView) lradio.getChildAt(0);
//                textView.setText(entity.getQuestionName());
//                llLayout.addView(lradio);
//                lradio.setTag(entity.getQuestionnaireQuestionId());
//                RadioGroup radioGroup = (RadioGroup) lradio.getChildAt(1);
//
//                QuestionDetailResponse detailResponse = FakeUtils.getQuestionDetail(FakeUtils.JNS_KEL_JSON);
//                if (detailResponse.getEntities().length > 2) {
//                    radioGroup.setOrientation(LinearLayout.VERTICAL);
//                } else {
//                    radioGroup.setOrientation(LinearLayout.HORIZONTAL);
//                }
//                for (int idx = 0; idx < detailResponse.getEntities().length; idx++) {
//                    client.android.psks.model.question.Entities item = detailResponse.getEntities()[idx];
//                    RadioButton radioButton = new RadioButton(getContext());
//                    radioButton.setTag(item.getMQuestionnaireQuestionDtl());
//                    radioButton.setId(Utils.generateViewId());
//                    radioButton.setText(item.getLabelText());
//                    radioGroup.addView(radioButton, idx);
//                }
//               // container.add(radioGroup);
//            } else if (entity.getQuestionType().equalsIgnoreCase("SECTION")) {
//                Log.i(TAG, "SECTION");
//                LinearLayout llsection = (LinearLayout) inflater.inflate(R.layout.comp_section, null);
//                llLayout.addView(llsection);
//            } else if (entity.getQuestionType().equalsIgnoreCase("DATE")) {
//                TextInputLayout textInputLayout = (TextInputLayout) inflater.inflate(R.layout.comp_date, null);
//                final TextInputEditText inputEditText = (TextInputEditText) textInputLayout.getEditText();
//                inputEditText.setTag(entity.getQuestionnaireQuestionId());
//                inputEditText.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        pickDate(inputEditText, getActivity());
//                    }
//                });
//                llLayout.addView(textInputLayout);
//               // container.add(inputEditText);
//
//            } else if (entity.getQuestionType().equalsIgnoreCase("TIME")) {
//                Log.i(TAG, "TIME");
//                TextInputLayout textInputLayout = (TextInputLayout) inflater.inflate(R.layout.comp_time, null);
//                final TextInputEditText inputEditText = (TextInputEditText) textInputLayout.getEditText();
//                inputEditText.setTag(entity.getQuestionnaireQuestionId());
//                inputEditText.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        pickTime(inputEditText, getContext());
//                    }
//                });
//                llLayout.addView(textInputLayout);
//               // container.add(inputEditText);
//            } else if (entity.getQuestionType().equalsIgnoreCase("SLIDER")) {
//                Log.i(TAG, "RATING");
//                LinearLayout llRatingBar = (LinearLayout) getLayoutInflater().inflate(R.layout.comp_start, null);
//                RatingBar ratingBar = (RatingBar) llRatingBar.getChildAt(0);
//                ratingBar.setTag(entity.getQuestionnaireQuestionId());
//                Log.i(TAG, "RATING" + entity.getLinearToNum());
//                ratingBar.setNumStars(Integer.parseInt(entity.getLinearToNum()));
//                ratingBar.setRating(Integer.parseInt(entity.getLinearStartNum()));
//
//                llLayout.addView(llRatingBar);
//               // container.add(ratingBar);
//
//
//            } else if (entity.getQuestionType().equalsIgnoreCase("LINEAR")) {
//                Log.i(TAG, "LINEAR");
//                LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.comp_radiogroup_linear, null);
//                TextView textView = (TextView) linearLayout.getChildAt(0);
//                textView.setText(entity.getQuestionName());
//                LinearLayout linearLayout2 = (LinearLayout) linearLayout.getChildAt(1);
//                RadioGroup radioGroup = (RadioGroup) linearLayout2.getChildAt(1);
//                for (int idx = 0; idx < Integer.parseInt(entity.getLinearToNum()); idx++) {
//                    RadioButton radioButton = (RadioButton) inflater.inflate(R.layout.comp_radio_text_ontop, null);
//                    int number = idx + 1;
//                    radioButton.setText(number + "");
//                    radioGroup.addView(radioButton);
//                }
//                llLayout.addView(linearLayout);
//               // container.add(radioGroup);
//            } else if (entity.getQuestionType().equalsIgnoreCase("IMAGE")) {
//                final LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.comp_upload, null);
//                final ImageView imageView = (ImageView) linearLayout.getChildAt(0);
//                imageView.setImageResource(R.drawable.ic_picture);
//                imageView.setTag(R.id.column,entity.getQuestionnaireQuestionId());
//
//                Button button = (Button) linearLayout.getChildAt(1);
//                button.setText(getString(R.string.chose_photo_file));
//                button.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        imagePicker = new ImagePicker.Builder(getActivity())
//                                .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
//                                .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
//                                .directory(ImagePicker.Directory.DEFAULT)
//                                .extension(ImagePicker.Extension.PNG)
//                                .scale(600, 600)
//                                .allowMultipleImages(false)
//                                .enableDebuggingMode(true)
//                                .build();
//                        selectedImageView = imageView;
//
//                    }
//                });
//                //container.add(imageView);
//                llLayout.addView(linearLayout);
//
//            } else if (entity.getQuestionType().equalsIgnoreCase("VIDEO")) {
//                final LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.comp_upload, null);
//                final ImageView imageView = (ImageView) linearLayout.getChildAt(0);
//                imageView.setImageResource(R.drawable.ic_youtube);
//                imageView.setTag(R.id.column,entity.getQuestionnaireQuestionId());
//                Button button = (Button) linearLayout.getChildAt(1);
//                button.setText(getString(R.string.chose_video_file));
//                button.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        videoPicker= new VideoPicker.Builder(getActivity())
//                                .mode(VideoPicker.Mode.CAMERA_AND_GALLERY)
//                                .directory(VideoPicker.Directory.DEFAULT)
//                                .extension(VideoPicker.Extension.MP4)
//                                .enableDebuggingMode(true)
//                                .build();
//                        selectedImageView = imageView;
//
//                    }
//                });
//                //container.add(imageView);
//                llLayout.addView(linearLayout);
//
//
//            } else if (entity.getQuestionType().equalsIgnoreCase("REFERENCE")) {
//
//                LinearLayout llradio = (LinearLayout) inflater.inflate(R.layout.comp_label_spinner, null);
//                TextView textView = (TextView) llradio.getChildAt(0);
//                textView.setText(entity.getQuestionName());
//                llLayout.addView(llradio);
//                llradio.setTag(entity.getQuestionnaireQuestionId());
//
//                if (entity.getReferenceValueType().equalsIgnoreCase("PROVINCE")) {
//                    //get Data For Spinner
//                    ProvinceResponse detailResponse = FakeReferences.getProvinsi();
//                    //create Adapater
//                    ProvinceAdapter dataAdapter = new ProvinceAdapter(getContext(), android.R.layout.simple_spinner_dropdown_item, detailResponse.getEntities());
//                    //createSpinner and set Adapater
//                    Spinner spinner = (Spinner) inflater.inflate(R.layout.comp_dropdown, null);
//                    spinner.setTag(entity.getQuestionnaireQuestionId());
//                    spinner.setAdapter(dataAdapter);
//                    llLayout.addView(spinner);
//                    //container.add(spinner);
//
//                } else if (entity.getReferenceValueType().equalsIgnoreCase("DISTRICT")) {
//                    //get Data For Spinner
//                    DistrictResponse detailResponse = FakeReferences.getDistrict();
//                    //create Adapater
//                    DistrictAdapter dataAdapter = new DistrictAdapter(getContext(), android.R.layout.simple_spinner_item, detailResponse.getEntities());
//                    //createSpinner and set Adapater
//                    Spinner spinner = (Spinner) inflater.inflate(R.layout.comp_dropdown, null);
//                    spinner.setTag(entity.getQuestionnaireQuestionId());
//                    spinner.setAdapter(dataAdapter);
//                    llLayout.addView(spinner);
//                    //container.add(spinner);
//
//
//                } else if (entity.getReferenceValueType().equalsIgnoreCase("SUBDISTRICT")) {
//                    //get Data For Spinner
//                    SubDistrictResponse detailResponse = FakeReferences.getSubDistrict();
//                    //create Adapater
//                    SubdistrictAdapter dataAdapter = new SubdistrictAdapter(getContext(), android.R.layout.simple_spinner_item, detailResponse.getEntities());
//                    //createSpinner and set Adapater
//                    Spinner spinner = (Spinner) inflater.inflate(R.layout.comp_dropdown, null);
//                    spinner.setTag(entity.getQuestionnaireQuestionId());
//                    spinner.setAdapter(dataAdapter);
//                    llLayout.addView(spinner);
////                    container.add(spinner);
//
//                } else if (entity.getReferenceValueType().equalsIgnoreCase("VILLAGE")) {
//                    //get Data For Spinner
//                    VillageResponse detailResponse = FakeReferences.getVillage();
//                    //create Adapater
//                    VillageAdapter dataAdapter = new VillageAdapter(getContext(), android.R.layout.simple_spinner_item, detailResponse.getEntities());
//                    //createSpinner and set Adapater
//                    Spinner spinner = (Spinner) inflater.inflate(R.layout.comp_dropdown, null);
//                    spinner.setTag(entity.getQuestionnaireQuestionId());
//                    spinner.setAdapter(dataAdapter);
//                    llLayout.addView(spinner);
////                    container.add(spinner);
//
//                }
//
//
//            }
//        }
//        return view;
//
//    }
//
//    @Override
//    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {
//        callback.goToNextStep();
//    }
//
//    @Override
//    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {
//        callback.complete();
//    }
//
//    @Override
//    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
//        callback.goToPrevStep();
//    }
//
//    @Nullable
//    @Override
//    public VerificationError verifyStep() {
//        return null;
//    }
//
//    @Override
//    public void onSelected() {
//        Log.i(TAG,"================================");
//
//        for (Entities e: entities){
//            Log.i(TAG,e.toString());
//        }
//        Log.i(TAG,"================================");
//
//    }
//
//    @Override
//    public void onError(@NonNull VerificationError error) {
//
//    }
}
