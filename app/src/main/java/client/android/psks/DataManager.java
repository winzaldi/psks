package client.android.psks;

import java.util.HashMap;

/**
 * Created by winzaldi on 11/25/17.
 */

public interface DataManager  {

    public void saveData(String key, String value);

    public HashMap getData();

}
