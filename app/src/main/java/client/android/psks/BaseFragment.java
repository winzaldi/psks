package client.android.psks;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TimePicker;

import net.alhazmy13.mediapicker.Image.ImagePicker;
import net.alhazmy13.mediapicker.Video.VideoPicker;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by winzaldi on 11/25/17.
 */

public class BaseFragment extends Fragment {
    
    private static final String TAG = BaseFragment.class.getSimpleName();

    protected List<View> container = new ArrayList<>();
    protected EditText selectedEditText;
    protected Calendar calendar;
    protected ImagePicker imagePicker;
    protected VideoPicker videoPicker;
    protected ImageView selectedImageView;




    // Date picker
    public void pickDate(EditText et, Context context) {
        if (et != null) {
            selectedEditText = et;
            new DatePickerDialog(context, datePickerListener, calendar
                    .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)).show();
        }
    }

    protected DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

            if (selectedEditText != null) {
                selectedEditText.setText(sdf.format(calendar.getTime()));
            }
        }

    };

    // Time picker
    public void pickTime(EditText et, Context context) {
        if (et != null) {
            selectedEditText = et;
            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(context, timePickerListener, hour, minute, true);//Yes 24 hour time
            mTimePicker.show();
        }
    }


    protected TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
            calendar.set(Calendar.HOUR, selectedHour);
            calendar.set(Calendar.MINUTE, selectedMinute);

            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");

            if (selectedEditText != null) {
                selectedEditText.setText(sdf.format(calendar.getTime()));
            }
        }
    };





    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        List<String> mPaths = null;
        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == FormStepperActivity.RESULT_OK) {
            mPaths = (List<String>) data.getSerializableExtra(ImagePicker.EXTRA_IMAGE_PATH);
            //Your Code
            selectedImageView.setImageBitmap(BitmapFactory.decodeFile(mPaths.get(0)));
            selectedImageView.setTag(R.id.value,mPaths.get(0));


        }else if (requestCode == VideoPicker.VIDEO_PICKER_REQUEST_CODE && resultCode == FormStepperActivity.RESULT_OK) {
            mPaths = (List<String>) data.getSerializableExtra(VideoPicker.EXTRA_VIDEO_PATH);
            Log.i(TAG,mPaths.get(0));
            Bitmap thumb = ThumbnailUtils.createVideoThumbnail(mPaths.get(0), MediaStore.Video.Thumbnails.MINI_KIND);
            selectedImageView.setImageBitmap(thumb);
            selectedImageView.setTag(R.id.value,mPaths.get(0));
        }

    }
}
