package client.android.psks;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;

import net.alhazmy13.mediapicker.Image.ImagePicker;

import java.util.List;

import client.android.psks.model.login.LoginResponse;
import client.android.psks.ui.kelembagaan.FragmentKelembagaan;
import client.android.psks.ui.login.LoginActivity;
import client.android.psks.ui.member.FragmentMember;
import client.android.psks.ui.survey.FragmentAssignmentList;
import client.android.psks.utils.PSKSConstanta;
import client.android.psks.utils.SharedPrefsUtils;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {



    public interface OnBackPressedListener {
        void doBack();
    }
    protected OnBackPressedListener onBackPressedListener;
    private static final String TAG = MainActivity.class.getName();
    private Button button;
    private CircleImageView profile_image;
    private TextView tv_username;
    private ImagePicker imagePicker;
    private LoginResponse user;
    private Toolbar toolbar;
    public NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View hView = navigationView.getHeaderView(0);



        user = new Gson().fromJson(SharedPrefsUtils.getFromPrefs(this, PSKSConstanta.LOGIN_OBJ, ""), LoginResponse.class);
        profile_image = hView.findViewById(R.id.profile_image);
        tv_username = hView.findViewById(R.id.tv_username);
        tv_username.setText(user.getEntity().getPsksName());

        String urlImage = SharedPrefsUtils.getFromPrefs(MainActivity.this,
                PSKSConstanta.IMG_PROFILE + user.getEntity().getPsksMemberId(), "");
        if (urlImage != null && urlImage.length() > 0) {
            profile_image.setImageBitmap(BitmapFactory.decodeFile(urlImage));
        }

        Log.i(TAG, (profile_image == null) + "");
        profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                imagePicker = new ImagePicker.Builder(MainActivity.this)
                        .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                        .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
                        .directory(ImagePicker.Directory.DEFAULT)
                        .extension(ImagePicker.Extension.PNG)
                        .scale(600, 600)
                        .allowMultipleImages(false)
                        .enableDebuggingMode(true)
                        .build();

            }
        });

        if (savedInstanceState == null) {
            MenuItem item = navigationView.getMenu().getItem(0);
            onNavigationItemSelected(item);
        }

        hideMenuKelembagaan();

    }

    private void hideMenuKelembagaan() {
        // hide and show menu kelembagaan depend on Partycipan Type
        if(user.getEntity().getParticipantType().equals("3")){
            Menu nav_Menu = navigationView.getMenu();
            nav_Menu.findItem(R.id.nav_kelembagaan).setVisible(true);
        }else{
            Menu nav_Menu = navigationView.getMenu();
            nav_Menu.findItem(R.id.nav_kelembagaan).setVisible(false);
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (onBackPressedListener != null) {
                onBackPressedListener.doBack();
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;
        String tag = "";

        if (id == R.id.nav_survey) {
            // Survey
            toolbar.setTitle(R.string.assignment);
            fragment = FragmentAssignmentList.newInstance();
            tag = "assignment";
        } else if (id == R.id.nav_member) {
            toolbar.setTitle(R.string.member);
            fragment = FragmentMember.newInstance();
            tag = "member";
        } else if (id == R.id.nav_kelembagaan) {
            toolbar.setTitle(R.string.kelembagaan);
            fragment = FragmentKelembagaan.newInstance();
            tag = "Kelembagaan";
        } else if (id == R.id.nav_sign_out) {
            SharedPrefsUtils.removeFromPrefs(getApplicationContext(), PSKSConstanta.NEKOT_APP);
            SharedPrefsUtils.removeFromPrefs(getApplicationContext(), PSKSConstanta.LOGIN_OBJ);
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }

        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame, fragment, tag).addToBackStack(tag);
            fragmentTransaction.commit();
        }


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        item.setChecked(true);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        List<String> mPaths = null;

        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }

        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {

            mPaths = (List<String>) data.getSerializableExtra(ImagePicker.EXTRA_IMAGE_PATH);
            SharedPrefsUtils.saveToPrefs(MainActivity.this, PSKSConstanta.IMG_PROFILE + user.getEntity().getPsksMemberId(), mPaths.get(0));
            profile_image.setImageBitmap(BitmapFactory.decodeFile(mPaths.get(0)));


        }

    }

    public void setOnBackPressedListener(OnBackPressedListener onBackPressedListener) {
        this.onBackPressedListener = onBackPressedListener;
    }

}
